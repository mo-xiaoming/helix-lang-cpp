# helix-lang-cpp

1. `nix-shell -A g` to enter dev environment, or using `nix-shell -A c` to use clang

2. `cmake -Bbuild -S. -GNinja -DCMAKE_BUILD_TYPE=ASanAndUBSan`

3. `cmake --build build&& ./build/src/mx-lib-test && ./build/src/helix s1.hlx` runs an example file

```
  4333/7  // not allowed, because 4333 * (1/7) == 0

  3.9 * 5.7f  // no allowed, float and double are different types

  a = 7

  b = 1.1f - (if true then 3.f else .5f)

  c = if (a gt 0) then 9 + a * 2 else 9 - a * 2
```

It first transforms it into an intermediate lisp-like language

Then does type checking

```
┌────────────────────┐
│   type checking    │
└────────────────────┘

(Mul (Int 4333) (Inv (Int 7)))
  -> ERROR: cannot inv a Int
(Mul (Double 3.9) (Float 5.7))
  -> ERROR: Double and Float are imcompatible
(Bind a (Int 7))
  -> Int
(Bind b (Add (Float 1.1) (Neg (If (Bool true) (Float 3.) (Float .5)))))
  -> Float
(Bind c (Add (If (Gt (Id a) (Int 0)) (Add (Int 9) (Mul (Id a) (Int 2))) (Int 9)) (Neg (Mul (Id a) (Int 2)))))
  -> Int
```

Finally, evaluation

```
┌────────────────────┐
│     evaluation     │
└────────────────────┘

(Mul (Int 4333) (Inv (Int 7)))
  -> ERROR: cannot inv a Int
(Mul (Double 3.9) (Float 5.7))
  -> ERROR: Double and Float are imcompatible
(Bind a (Int 7))
  -> 7::Int
(Bind b (Add (Float 1.1) (Neg (If (Bool true) (Float 3.) (Float .5)))))
  -> -1.900000::Float
(Bind c (Add (If (Gt (Id a) (Int 0)) (Add (Int 9) (Mul (Id a) (Int 2))) (Int 9)) (Neg (Mul (Id a) (Int 2)))))
  -> 9::Int
```
