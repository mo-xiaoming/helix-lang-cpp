{
  inputs = { nixpkgs.url = "github:nixos/nixpkgs"; };

  outputs = { self, nixpkgs }:
  let
    pkgs = nixpkgs.legacyPackages.x86_64-linux;
  in {
    packages.x86_64-linux = {
      gcc-pkg = (pkgs.callPackage ./. { nixpkgs=pkgs; }).g;
      clang-pkg = (pkgs.callPackage ./. { nixpkgs=pkgs; }).c;
    };

    defaultPackage.x86_64-linux = self.packages.x86_64-linux.gcc-pkg;
  };
}
