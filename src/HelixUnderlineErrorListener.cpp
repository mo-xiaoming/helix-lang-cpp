#include "HelixUnderlineErrorListener.hpp"

#include "Utils.hpp"

#include <spdlog/spdlog.h>

void UnderlineErrorListener::syntaxError(antlr4::Recognizer *recognizer,
                                         antlr4::Token *offendingSymbol,
                                         size_t line, size_t charPositionInLine,
                                         const std::string &msg,
                                         std::exception_ptr e) {
  const std::string filename = recognizer->getInputStream()->getSourceName();
  const std::string cu =
      dynamic_cast<antlr4::CommonTokenStream *>(recognizer->getInputStream())
          ->getTokenSource()
          ->getInputStream()
          ->toString();
  const auto lines = splitLines(cu);
  fmt::print(stderr, FMT_STRING("{}:{}:{}: error: {}\n"), filename, line,
             charPositionInLine + 1, msg);
  fmt::print(stderr, FMT_STRING("  |{}\n"), lines[line - 1]);
  fmt::print(
      stderr, FMT_STRING("  {:<{}}{:^<{}}\n"), '|', charPositionInLine + 1, '^',
      (offendingSymbol->getStopIndex() - offendingSymbol->getStartIndex() + 1));

  try {
    if (e) {
      std::rethrow_exception(e);
    }
  } catch (const std::exception &ex) {
    fmt::print(stderr, FMT_STRING("exception: {}\n"), ex.what());
  }
}
