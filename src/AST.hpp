#pragma once

#include <fmt/format.h>

#include <any>
#include <array>
#include <cassert>
#include <functional>
#include <map>
#include <string>
#include <string_view>
#include <unordered_map>
#include <variant>

using namespace std::literals; // NOLINT

template <typename T> using CPtr = std::shared_ptr<const T>;
template <typename T, typename... Args>
[[nodiscard]] inline CPtr<T> MkCPtr(Args &&...args) {
  return std::make_shared<const T>(std::forward<Args>(args)...);
}

struct [[nodiscard]] PrimType {
  enum class Enum : std::int8_t { Bool, Int, Float, Double };
  PrimType(const PrimType &) = default;
  PrimType(PrimType &&) noexcept = default;
  PrimType &operator=(PrimType &&) &noexcept = default;
  PrimType &operator=(const PrimType &) & = default;
  virtual ~PrimType() = default;

  [[nodiscard]] virtual std::string_view toString() const noexcept = 0;

  [[nodiscard]] virtual bool canNeg() const noexcept { return false; }
  [[nodiscard]] virtual bool canInv() const noexcept { return false; }
  [[nodiscard]] virtual bool canDivByZero() const noexcept { return false; }
  [[nodiscard]] virtual bool canMul() const noexcept { return false; }
  [[nodiscard]] virtual bool canAdd() const noexcept { return false; }
  [[nodiscard]] virtual bool canCompare() const noexcept { return false; }
  [[nodiscard]] virtual bool canEqual() const noexcept { return false; }
  [[nodiscard]] virtual bool canLogical() const noexcept { return false; }

  [[nodiscard]] virtual Enum toEnum() const noexcept = 0;

  friend bool operator==(const PrimType &lhs, const PrimType &rhs) noexcept {
    return lhs.toEnum() == rhs.toEnum();
  }
  friend bool operator!=(const PrimType &lhs, const PrimType &rhs) noexcept {
    return !(lhs == rhs);
  }

protected:
  PrimType() = default;
};

struct [[nodiscard]] BoolType : PrimType {
  static CPtr<PrimType> inst() {
    static auto r = MkCPtr<BoolType>();
    return r;
  }
  [[nodiscard]] std::string_view toString() const noexcept override {
    return "Bool"sv;
  }

  [[nodiscard]] bool canLogical() const noexcept override { return true; }
  [[nodiscard]] bool canEqual() const noexcept override { return true; }

  [[nodiscard]] Enum toEnum() const noexcept override { return Enum::Bool; }
};

struct [[nodiscard]] IntType : PrimType {
  static CPtr<PrimType> inst() {
    static auto r = MkCPtr<IntType>();
    return r;
  }
  [[nodiscard]] std::string_view toString() const noexcept override {
    return "Int"sv;
  }

  [[nodiscard]] bool canNeg() const noexcept override { return true; }
  [[nodiscard]] bool canMul() const noexcept override { return true; }
  [[nodiscard]] bool canAdd() const noexcept override { return true; }
  [[nodiscard]] bool canCompare() const noexcept override { return true; }
  [[nodiscard]] bool canEqual() const noexcept override { return true; }

  [[nodiscard]] Enum toEnum() const noexcept override { return Enum::Int; }
};

struct [[nodiscard]] FloatType : PrimType {
  static CPtr<PrimType> inst() {
    static auto r = MkCPtr<FloatType>();
    return r;
  }
  [[nodiscard]] std::string_view toString() const noexcept override {
    return "Float"sv;
  }

  [[nodiscard]] bool canNeg() const noexcept override { return true; }
  [[nodiscard]] bool canInv() const noexcept override { return true; }
  [[nodiscard]] bool canDivByZero() const noexcept override { return true; }
  [[nodiscard]] bool canMul() const noexcept override { return true; }
  [[nodiscard]] bool canAdd() const noexcept override { return true; }
  [[nodiscard]] bool canCompare() const noexcept override { return true; }

  [[nodiscard]] Enum toEnum() const noexcept override { return Enum::Float; }
};

struct [[nodiscard]] DoubleType : PrimType {
  static CPtr<PrimType> inst() {
    static auto r = MkCPtr<DoubleType>();
    return r;
  }
  [[nodiscard]] std::string_view toString() const noexcept override {
    return "Double"sv;
  }

  [[nodiscard]] bool canNeg() const noexcept override { return true; }
  [[nodiscard]] bool canInv() const noexcept override { return true; }
  [[nodiscard]] bool canDivByZero() const noexcept override { return true; }
  [[nodiscard]] bool canMul() const noexcept override { return true; }
  [[nodiscard]] bool canAdd() const noexcept override { return true; }
  [[nodiscard]] bool canCompare() const noexcept override { return true; }

  [[nodiscard]] Enum toEnum() const noexcept override { return Enum::Double; }
};

struct Expr;
struct [[nodiscard]] ExprVisitor {
  ExprVisitor() = default;
  ExprVisitor(const ExprVisitor &) = default;
  ExprVisitor(ExprVisitor &&) noexcept = default;
  auto operator=(ExprVisitor &&) &noexcept -> ExprVisitor & = default;
  auto operator=(const ExprVisitor &) & -> ExprVisitor & = default;
  virtual ~ExprVisitor() = default;

  virtual std::any visitBool(const std::string &v) = 0;
  virtual std::any visitInt(const std::string &v) = 0;
  virtual std::any visitFloat(const std::string &v) = 0;
  virtual std::any visitDouble(const std::string &v) = 0;
  virtual std::any visitNot(const Expr &e) = 0;
  virtual std::any visitNeg(const Expr &e) = 0;
  virtual std::any visitInv(const Expr &e) = 0;
  virtual std::any visitMul(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitAdd(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitOr(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitAnd(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitNe(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitLt(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitGt(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitEq(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitLe(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitGe(const Expr &e0, const Expr &e1) = 0;
  virtual std::any visitId(const std::string &name) = 0;
  virtual std::any visitBind(const std::string &name, const Expr &e) = 0;
  virtual std::any visitIf(const Expr &c, const Expr &t, const Expr &f) = 0;
};

struct [[nodiscard]] Expr {
  Expr() = default;
  Expr(const Expr &) = delete;
  Expr(Expr &&) = delete;
  auto operator=(Expr &&) &noexcept -> Expr & = default;
  auto operator=(const Expr &) & -> Expr & = default;
  virtual ~Expr() = default;

  [[nodiscard]] virtual std::string toString() const = 0;
  virtual std::any accept(ExprVisitor &visitor) const = 0;
};

#define PRIM_EXPR_DEFINE(n)                                                    \
  struct [[nodiscard]] n##Expr : Expr {                                        \
    explicit n##Expr(std::string v) : v_(std::move(v)) {}                      \
                                                                               \
    [[nodiscard]] std::string toString() const override {                      \
      return fmt::format(FMT_STRING("(" #n " {})"), v_);                       \
    }                                                                          \
    std::any accept(ExprVisitor &visitor) const override {                     \
      return visitor.visit##n(v_);                                             \
    }                                                                          \
                                                                               \
  private:                                                                     \
    const std::string v_;                                                      \
  }

PRIM_EXPR_DEFINE(Bool);
PRIM_EXPR_DEFINE(Int);
PRIM_EXPR_DEFINE(Float);
PRIM_EXPR_DEFINE(Double);

#define UNARY_EXPR_DEFINE(n)                                                   \
  struct [[nodiscard]] n##Expr : Expr {                                        \
    explicit n##Expr(CPtr<Expr> e) : e_(std::move(e)) {}                       \
                                                                               \
    [[nodiscard]] std::string toString() const override {                      \
      return fmt::format(FMT_STRING("(" #n " {})"), e_->toString());           \
    }                                                                          \
    std::any accept(ExprVisitor &visitor) const override {                     \
      return visitor.visit##n(*e_);                                            \
    }                                                                          \
                                                                               \
  private:                                                                     \
    const CPtr<Expr> e_;                                                       \
  }

UNARY_EXPR_DEFINE(Neg);
UNARY_EXPR_DEFINE(Inv);
UNARY_EXPR_DEFINE(Not);

#define BINARY_EXPR_DEFINE(n)                                                  \
  struct [[nodiscard]] n##Expr : Expr {                                        \
    explicit n##Expr(CPtr<Expr> e0, CPtr<Expr> e1)                             \
        : e0_(std::move(e0)), e1_(std::move(e1)) {}                            \
                                                                               \
    [[nodiscard]] std::string toString() const override {                      \
      return fmt::format(FMT_STRING("(" #n " {} {})"), e0_->toString(),        \
                         e1_->toString());                                     \
    }                                                                          \
    std::any accept(ExprVisitor &visitor) const override {                     \
      return visitor.visit##n(*e0_, *e1_);                                     \
    }                                                                          \
                                                                               \
  private:                                                                     \
    const CPtr<Expr> e0_;                                                      \
    const CPtr<Expr> e1_;                                                      \
  }

BINARY_EXPR_DEFINE(Add);
BINARY_EXPR_DEFINE(Mul);
BINARY_EXPR_DEFINE(Or);
BINARY_EXPR_DEFINE(And);
BINARY_EXPR_DEFINE(Ne);
BINARY_EXPR_DEFINE(Lt);
BINARY_EXPR_DEFINE(Gt);
BINARY_EXPR_DEFINE(Eq);
BINARY_EXPR_DEFINE(Le);
BINARY_EXPR_DEFINE(Ge);

struct [[nodiscard]] IdExpr : Expr {
  explicit IdExpr(std::string name) : name_(std::move(name)) {}

  [[nodiscard]] std::string toString() const override {
    return fmt::format(FMT_STRING("(Id {})"), name_);
  }
  std::any accept(ExprVisitor &visitor) const override {
    return visitor.visitId(name_);
  }

private:
  const std::string name_;
};

struct [[nodiscard]] BindExpr : Expr {
  explicit BindExpr(std::string name, CPtr<Expr> e)
      : name_(std::move(name)), e_(std::move(e)) {}

  [[nodiscard]] std::string toString() const override {
    return fmt::format(FMT_STRING("(Bind {} {})"), name_, e_->toString());
  }
  std::any accept(ExprVisitor &visitor) const override {
    return visitor.visitBind(name_, *e_);
  }

private:
  const std::string name_;
  const CPtr<Expr> e_;
};

struct [[nodiscard]] IfExpr : Expr {
  explicit IfExpr(CPtr<Expr> c, CPtr<Expr> t, CPtr<Expr> f)
      : c_(std::move(c)), t_(std::move(t)), f_(std::move(f)) {}

  [[nodiscard]] std::string toString() const override {
    return fmt::format(FMT_STRING("(If {} {} {})"), c_->toString(),
                       t_->toString(), f_->toString());
  }

  std::any accept(ExprVisitor &visitor) const override {
    return visitor.visitIf(*c_, *t_, *f_);
  }

private:
  const CPtr<Expr> c_;
  const CPtr<Expr> t_;
  const CPtr<Expr> f_;
};

template <typename S, typename F, typename Fn>
std::any checkError(ExprVisitor &visitor, const Expr &e, Fn &&f) {
  std::any r = e.accept(visitor);
  if (std::any_cast<F>(&r) != nullptr) {
    return r;
  }
  return std::forward<Fn>(f)(std::any_cast<S>(r));
}

// returns either Error or CPtr<PrimType>
struct [[nodiscard]] TypeCheckVisitor : ExprVisitor {
  struct [[nodiscard]] Error {
    Error() = default;
    explicit Error(std::string msg) : msg_(std::move(msg)) {}

    [[nodiscard]] std::string what() const &&noexcept { return msg_; }

    [[nodiscard]] const std::string &what() const &noexcept { return msg_; }

  private:
    std::string msg_;
  };

  using Success = CPtr<PrimType>;
  using Failure = Error;

  std::any visitBool(const std::string & /*v*/) override {
    return BoolType::inst();
  }
  std::any visitInt(const std::string & /*v*/) override {
    return IntType::inst();
  }
  std::any visitFloat(const std::string & /*v*/) override {
    return FloatType::inst();
  }
  std::any visitDouble(const std::string & /*v*/) override {
    return DoubleType::inst();
  }
  std::any visitNot(const Expr &e) override {
    return checkError<Success, Failure>(
        *this, e, [](const Success &t) -> std::any {
          if (t->canLogical()) {
            return t;
          }
          return Failure(
              fmt::format(FMT_STRING("cannot not a {}"), t->toString()));
        });
  }
  std::any visitNeg(const Expr &e) override {
    return checkError<Success, Failure>(
        *this, e, [](const Success &t) -> std::any {
          if (t->canNeg()) {
            return t;
          }
          return Failure(
              fmt::format(FMT_STRING("cannot neg a {}"), t->toString()));
        });
  }
  std::any visitInv(const Expr &e) override {
    return checkError<Success, Failure>(
        *this, e, [](const Success &t) -> std::any {
          if (t->canInv()) {
            return t;
          }
          return Failure(
              fmt::format(FMT_STRING("cannot inv a {}"), t->toString()));
        });
  }
  std::any visitMul(const Expr &e0, const Expr &e1) override {
    return checkError2(
        e0, e1, [](const Success &t0, const Success & /*t1*/) -> std::any {
          if (t0->canMul()) {
            return t0;
          }
          return Failure(fmt::format(FMT_STRING("cannot mul between two {}s"),
                                     t0->toString()));
        });
  }
  std::any visitAdd(const Expr &e0, const Expr &e1) override {
    return checkError2(
        e0, e1, [](const Success &t0, const Success & /*t1*/) -> std::any {
          if (t0->canAdd()) {
            return t0;
          }
          return Failure(fmt::format(FMT_STRING("cannot add between two {}s"),
                                     t0->toString()));
        });
  }
  std::any visitOr(const Expr &e0, const Expr &e1) override {
    return checkLogical(e0, e1, "or");
  }
  std::any visitAnd(const Expr &e0, const Expr &e1) override {
    return checkLogical(e0, e1, "and");
  }
  std::any visitNe(const Expr &e0, const Expr &e1) override {
    return checkCompare(e0, e1, "ne");
  }
  std::any visitLt(const Expr &e0, const Expr &e1) override {
    return checkCompare(e0, e1, "lt");
  }
  std::any visitGt(const Expr &e0, const Expr &e1) override {
    return checkCompare(e0, e1, "gt");
  }
  std::any visitEq(const Expr &e0, const Expr &e1) override {
    return checkCompare(e0, e1, "eq");
  }
  std::any visitLe(const Expr &e0, const Expr &e1) override {
    return checkCompare(e0, e1, "le");
  }
  std::any visitGe(const Expr &e0, const Expr &e1) override {
    return checkCompare(e0, e1, "ge");
  }
  std::any visitId(const std::string &name) override {
    if (const auto it = names_.find(name); it != names_.cend()) {
      if (std::holds_alternative<Failure>(it->second)) {
        return std::get<Failure>(it->second);
      }
      if (std::holds_alternative<Success>(it->second)) {
        return std::get<Success>(it->second);
      }
      assert(false); // NOLINT
    }
    return Failure(fmt::format(FMT_STRING("{} not defined"), name));
  }
  std::any visitBind(const std::string &name, const Expr &e) override {
    std::any t = e.accept(*this);
    auto *ts = std::any_cast<Success>(&t);
    if (ts != nullptr) {
      names_[name] = *ts;
      return *ts;
    }
    names_[name] = std::any_cast<Failure>(t);
    return std::any_cast<Failure>(t);
  }
  std::any visitIf(const Expr &c, const Expr &t, const Expr &f) override {
    std::any ct = c.accept(*this);
    const auto *cts = std::any_cast<Success>(&ct);
    if (cts == nullptr) {
      return ct;
    }
    if ((*cts) != BoolType::inst()) {
      return Failure(fmt::format(FMT_STRING("if condition cannot be {}"),
                                 (*cts)->toString()));
    }

    return checkError2(
        t, f, [](const Success &tt, const Success & /*ft*/) { return tt; });
  }

private:
  template <typename Fn>
  std::any checkError2(const Expr &e0, const Expr &e1, Fn &&f) {
    return checkError<Success, Failure>(
        *this, e0, [&](const Success &t0) -> std::any {
          return checkError<Success, Failure>(
              *this, e1, [&](const Success &t1) -> std::any {
                if (t0 != t1) {
                  return Failure(
                      fmt::format(FMT_STRING("{} and {} are imcompatible"),
                                  t0->toString(), t1->toString()));
                }
                return std::forward<Fn>(f)(t0, t1);
              });
        });
  }
  std::any checkLogical(const Expr &e0, const Expr &e1, std::string_view op) {
    return checkError2(
        e0, e1, [op](const Success &t0, const Success & /*t1*/) -> std::any {
          if (t0->canLogical()) {
            return BoolType::inst();
          }
          return Failure(fmt::format(FMT_STRING("cannot {} between two {}s"),
                                     op, t0->toString()));
        });
  }
  std::any checkCompare(const Expr &e0, const Expr &e1, std::string_view op) {
    return checkError2(
        e0, e1, [op](const Success &t0, const Success & /*t1*/) -> std::any {
          if (t0->canCompare()) {
            return BoolType::inst();
          }
          return Failure(fmt::format(FMT_STRING("cannot {} between two {}s"),
                                     op, t0->toString()));
        });
  }

  using TypeErrorSum = std::variant<Failure, Success>;
  std::unordered_map<std::string, TypeErrorSum> names_;
};

template <class... Ts> struct overloaded : Ts... { using Ts::operator()...; };
template <class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

// returns either Error or EvalResult
struct [[nodiscard]] EvalVisitor : ExprVisitor {
  struct [[nodiscard]] Error {
    Error() = default;
    explicit Error(std::string msg) : msg_(std::move(msg)) {}

    [[nodiscard]] std::string what() const &&noexcept { return msg_; }

    [[nodiscard]] const std::string &what() const &noexcept { return msg_; }

  private:
    std::string msg_;
  };

  using NativeType = std::variant<int, float, double, bool>;
  struct [[nodiscard]] EvalResult {
    NativeType nativeValue;
    CPtr<PrimType> primType;
  };

  using Success = EvalResult;
  using Failure = Error;

  std::any visitBool(const std::string &v) override {
    return Success{v == "true", BoolType::inst()};
  }
  std::any visitInt(const std::string &v) override {
    return toNum<int>(IntType::inst(), v);
  }
  std::any visitFloat(const std::string &v) override {
    return toNum<float>(FloatType::inst(), v);
  }
  std::any visitDouble(const std::string &v) override {
    return toNum<double>(DoubleType::inst(), v);
  }
  std::any visitNot(const Expr &e) override {
    return checkError<Success, Failure>(
        *this, e, [](const Success &er) -> std::any {
          if (er.primType->canLogical()) {
            return Failure(fmt::format(FMT_STRING("cannot not a {}"),
                                       er.primType->toString()));
          }
          return Success{!std::get<bool>(er.nativeValue), BoolType::inst()};
        });
  }
  std::any visitNeg(const Expr &e) override {
    return checkError<Success, Failure>(
        *this, e, [](const Success &er) -> std::any {
          switch (er.primType->toEnum()) {
          case PrimType::Enum::Int:
            assert(er.primType->canNeg()); // NOLINT
            return Success{-1 * std::get<int>(er.nativeValue), IntType::inst()};
          case PrimType::Enum::Float:
            assert(er.primType->canNeg()); // NOLINT
            return Success{-1.f * std::get<float>(er.nativeValue),
                           FloatType::inst()};
          case PrimType::Enum::Double:
            assert(er.primType->canNeg()); // NOLINT
            return Success{-1. * std::get<double>(er.nativeValue),
                           DoubleType::inst()};
          case PrimType::Enum::Bool:
            assert(!er.primType->canNeg()); // NOLINT
            return Failure(fmt::format(FMT_STRING("cannot neg a {}"),
                                       er.primType->toString()));
          default:
            return Failure(fmt::format(FMT_STRING("neg an unknown type {}"),
                                       er.primType->toString()));
          }
        });
  }
  std::any visitInv(const Expr &e) override {
    return checkError<Success, Failure>(
        *this, e, [](const Success &er) -> std::any {
          switch (er.primType->toEnum()) {
          case PrimType::Enum::Float:
            assert(er.primType->canInv()); // NOLINT
            return Success{-1.f / std::get<float>(er.nativeValue),
                           FloatType::inst()};
          case PrimType::Enum::Double:
            assert(er.primType->canInv()); // NOLINT
            return Success{-1. / std::get<double>(er.nativeValue),
                           DoubleType::inst()};
          case PrimType::Enum::Int:
          case PrimType::Enum::Bool:
            assert(!er.primType->canInv()); // NOLINT
            return Failure(fmt::format(FMT_STRING("cannot inv a {}"),
                                       er.primType->toString()));
          default:
            return Failure(fmt::format(FMT_STRING("inv an unknown type {}"),
                                       er.primType->toString()));
          }
        });
  }
  std::any visitMul(const Expr &e0, const Expr &e1) override {
    return visit2(
        e0, e1, "mul", std::mem_fn(&PrimType::canMul),
        [](const NativeType &a, const NativeType &b) {
          return std::visit(
              [](auto i, decltype(i) j) -> NativeType { return i * j; }, a, b);
        });
  }
  std::any visitAdd(const Expr &e0, const Expr &e1) override {
    return visit2(
        e0, e1, "add", std::mem_fn(&PrimType::canAdd),
        [](const NativeType &a, const NativeType &b) {
          return std::visit(
              [](auto i, decltype(i) j) -> NativeType { return i + j; }, a, b);
        });
  }
  std::any visitOr(const Expr &e0, const Expr &e1) override {
    return visit2(e0, e1, "or", std::mem_fn(&PrimType::canLogical),
                  [](const NativeType &a, const NativeType &b) {
                    return std::visit(
                        [](auto i, auto j) -> NativeType {
                          using it = std::decay_t<decltype(i)>;
                          using jt = std::decay_t<decltype(j)>;
                          if constexpr (std::is_same_v<it, bool> &&
                                        std::is_same_v<jt, bool>) {
                            return i || j;
                          } else {
                            assert(false); // NOLINT
                            return false;
                          }
                        },
                        a, b);
                  });
  }
  std::any visitAnd(const Expr &e0, const Expr &e1) override {
    return visit2(e0, e1, "and", std::mem_fn(&PrimType::canLogical),
                  [](const NativeType &a, const NativeType &b) {
                    return std::visit(
                        [](auto i, auto j) -> NativeType {
                          using it = std::decay_t<decltype(i)>;
                          using jt = std::decay_t<decltype(j)>;
                          if constexpr (std::is_same_v<it, bool> &&
                                        std::is_same_v<jt, bool>) {
                            return i && j;
                          } else {
                            assert(false); // NOLINT
                            return false;
                          }
                        },
                        a, b);
                  });
  }
  std::any visitNe(const Expr &e0, const Expr &e1) override {
    return visit2(e0, e1, "ne", std::mem_fn(&PrimType::canEqual),
                  [](const NativeType &a, const NativeType &b) {
                    return std::visit(
                        [](auto i, auto j) -> NativeType {
                          using it = std::decay_t<decltype(i)>;
                          using jt = std::decay_t<decltype(j)>;
                          if constexpr (std::is_floating_point_v<it> ||
                                        std::is_floating_point_v<jt>) {
                            assert(false); // NOLINT
                            return false;
                          } else {
                            return i != j;
                          }
                        },
                        a, b);
                  });
  }
  std::any visitLt(const Expr &e0, const Expr &e1) override {
    return visitIfCond(
        e0, e1, "lt", std::mem_fn(&PrimType::canCompare),
        [](const NativeType &a, const NativeType &b) {
          return std::visit(
              [](auto i, decltype(i) j) -> NativeType { return i < j; }, a, b);
        });
  }
  std::any visitGt(const Expr &e0, const Expr &e1) override {
    return visitIfCond(
        e0, e1, "gt", std::mem_fn(&PrimType::canCompare),
        [](const NativeType &a, const NativeType &b) {
          return std::visit(
              [](auto i, decltype(i) j) -> NativeType { return i > j; }, a, b);
        });
  }
  std::any visitEq(const Expr &e0, const Expr &e1) override {
    return visitIfCond(e0, e1, "eq", std::mem_fn(&PrimType::canEqual),
                       [](const NativeType &a, const NativeType &b) {
                         return std::visit(
                             [](auto i, auto j) -> NativeType {
                               using it = std::decay_t<decltype(i)>;
                               using jt = std::decay_t<decltype(j)>;
                               if constexpr (std::is_floating_point_v<it> ||
                                             std::is_floating_point_v<jt>) {
                                 assert(false); // NOLINT
                                 return false;
                               } else {
                                 return i == j;
                               }
                             },
                             a, b);
                       });
  }
  std::any visitLe(const Expr &e0, const Expr &e1) override {
    return visitIfCond(
        e0, e1, "le", std::mem_fn(&PrimType::canCompare),
        [](const NativeType &a, const NativeType &b) {
          return std::visit(
              [](auto i, decltype(i) j) -> NativeType { return i <= j; }, a, b);
        });
  }
  std::any visitGe(const Expr &e0, const Expr &e1) override {
    return visitIfCond(
        e0, e1, "ge", std::mem_fn(&PrimType::canCompare),
        [](const NativeType &a, const NativeType &b) {
          return std::visit(
              [](auto i, decltype(i) j) -> NativeType { return i >= j; }, a, b);
        });
  }
  std::any visitId(const std::string &name) override {
    if (const auto it = names_.find(name); it != names_.cend()) {
      if (std::holds_alternative<Failure>(it->second)) {
        return std::get<Failure>(it->second);
      }
      if (std::holds_alternative<Success>(it->second)) {
        return std::get<Success>(it->second);
      }
      assert(false); // NOLINT
    }
    return Failure(fmt::format(FMT_STRING("{} not defined"), name));
  }
  std::any visitBind(const std::string &name, const Expr &e) override {
    std::any t = e.accept(*this);
    auto *ts = std::any_cast<Success>(&t);
    if (ts != nullptr) {
      names_[name] = *ts;
      return *ts;
    }
    names_[name] = std::any_cast<Failure>(t);
    return std::any_cast<Failure>(t);
  }
  std::any visitIf(const Expr &c, const Expr &t, const Expr &f) override {
    std::any ct = c.accept(*this);
    const auto *cts = std::any_cast<Success>(&ct);
    if (cts == nullptr) {
      return ct;
    }
    if (!cts->primType->canLogical()) {
      return Failure(fmt::format(FMT_STRING("if condition cannot be {}"),
                                 cts->primType->toString()));
    }

    if (std::get<bool>(cts->nativeValue)) {
      return t.accept(*this);
    }
    return f.accept(*this);
  }

private:
  template <typename T>
  std::any toNum(const CPtr<PrimType> &t, const std::string &s) {
    const auto e = Failure(
        fmt::format(FMT_STRING("{} is not a valid {}"), s, t->toString()));
    try {
      if constexpr (std::is_same_v<T, int>) {
        return Success{std::stoi(s, nullptr), t};
      } else if constexpr (std::is_same_v<T, float>) {
        return Success{std::stof(s, nullptr), t};
      } else if constexpr (std::is_same_v<T, double>) {
        return Success{std::stod(s, nullptr), t};
      }
      return e;
    } catch (const std::out_of_range &) {
      return Failure(fmt::format(FMT_STRING("{} out of range of type {}"), s,
                                 t->toString()));
    }
  }
  template <typename Fn>
  std::any checkError2(const Expr &e0, const Expr &e1, Fn &&f) {
    return checkError<Success, Failure>(
        *this, e0, [&](const Success &er0) -> std::any {
          return checkError<Success, Failure>(
              *this, e1, [&](const Success &er1) -> std::any {
                if (er0.primType != er1.primType) {
                  return Failure(fmt::format(
                      FMT_STRING("{} and {} are imcompatible"),
                      er0.primType->toString(), er1.primType->toString()));
                }
                return std::forward<Fn>(f)(er0, er1);
              });
        });
  }

  template <typename Pred, typename Op>
  std::any visit2(const Expr &e0, const Expr &e1, std::string_view s, Pred pred,
                  Op op) {
    return checkError2(
        e0, e1,
        [s, pred, op](const Success &er0, const Success &er1) -> std::any {
          if (!pred(er0.primType)) {
            return Failure(fmt::format(FMT_STRING("cannot {} two {}s"), s,
                                       er0.primType->toString()));
          }
          return Success{op(er0.nativeValue, er1.nativeValue), er0.primType};
        });
  }

  template <typename Pred, typename Op>
  std::any visitIfCond(const Expr &e0, const Expr &e1, std::string_view s,
                       Pred pred, Op op) {
    std::any r = visit2(e0, e1, s, pred, op);
    if (std::any_cast<Error>(&r) != nullptr) {
      return r;
    }
    const auto rs = std::any_cast<Success>(r);
    return Success{rs.nativeValue, BoolType::inst()};
  }

  using ValueErrorSum = std::variant<Failure, Success>;
  std::unordered_map<std::string, ValueErrorSum> names_;
};
