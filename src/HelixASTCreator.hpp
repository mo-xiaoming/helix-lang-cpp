#pragma once

#include "HelixLispyBaseVisitor.h"

struct HelixASTCreator : HelixLispyBaseVisitor {
  antlrcpp::Any visitCu(HelixLispyParser::CuContext *ctx) override;
  antlrcpp::Any visitIf(HelixLispyParser::IfContext *ctx) override;
  antlrcpp::Any visitBind(HelixLispyParser::BindContext *ctx) override;
  antlrcpp::Any visitId(HelixLispyParser::IdContext *ctx) override;
  antlrcpp::Any
  visitBinaryOp(HelixLispyParser::BinaryOpContext *ctx) override;
  antlrcpp::Any
  visitUnaryOp(HelixLispyParser::UnaryOpContext *ctx) override;
  antlrcpp::Any visitFloat(HelixLispyParser::FloatContext *ctx) override;
  antlrcpp::Any visitDouble(HelixLispyParser::DoubleContext *ctx) override;
  antlrcpp::Any visitInt(HelixLispyParser::IntContext *ctx) override;
  antlrcpp::Any visitBool(HelixLispyParser::BoolContext *ctx) override;
};

