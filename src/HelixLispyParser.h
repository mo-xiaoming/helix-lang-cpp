
// Generated from HelixLispy.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"




class  HelixLispyParser : public antlr4::Parser {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, T__8 = 9, T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, 
    T__14 = 15, T__15 = 16, T__16 = 17, T__17 = 18, T__18 = 19, T__19 = 20, 
    BIND = 21, MUL = 22, DIV = 23, ADD = 24, SUB = 25, GE = 26, LE = 27, 
    EQ = 28, GT = 29, LT = 30, NE = 31, NOT = 32, AND = 33, OR = 34, LB = 35, 
    RB = 36, LP = 37, RP = 38, BOOL = 39, ID = 40, FLOAT = 41, INT = 42, 
    DOUBLE = 43, STRING = 44, LINE_COMMENT = 45, COMMENT = 46, NL = 47, 
    WS = 48
  };

  enum {
    RuleCu = 0, RuleExpr = 1
  };

  HelixLispyParser(antlr4::TokenStream *input);
  ~HelixLispyParser();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class CuContext;
  class ExprContext; 

  class  CuContext : public antlr4::ParserRuleContext {
  public:
    CuContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *EOF();
    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    std::vector<antlr4::tree::TerminalNode *> NL();
    antlr4::tree::TerminalNode* NL(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CuContext* cu();

  class  ExprContext : public antlr4::ParserRuleContext {
  public:
    ExprContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    ExprContext() = default;
    void copyFrom(ExprContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  FloatContext : public ExprContext {
  public:
    FloatContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *LP();
    antlr4::tree::TerminalNode *DOUBLE();
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  BindContext : public ExprContext {
  public:
    BindContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *LP();
    antlr4::tree::TerminalNode *ID();
    ExprContext *expr();
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  UnaryOpContext : public ExprContext {
  public:
    UnaryOpContext(ExprContext *ctx);

    antlr4::Token *op = nullptr;
    antlr4::tree::TerminalNode *LP();
    ExprContext *expr();
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  BoolContext : public ExprContext {
  public:
    BoolContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *LP();
    antlr4::tree::TerminalNode *BOOL();
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  IdContext : public ExprContext {
  public:
    IdContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *LP();
    antlr4::tree::TerminalNode *ID();
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  IfContext : public ExprContext {
  public:
    IfContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *LP();
    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  DoubleContext : public ExprContext {
  public:
    DoubleContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *LP();
    antlr4::tree::TerminalNode *DOUBLE();
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  IntContext : public ExprContext {
  public:
    IntContext(ExprContext *ctx);

    antlr4::tree::TerminalNode *LP();
    antlr4::tree::TerminalNode *INT();
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  BinaryOpContext : public ExprContext {
  public:
    BinaryOpContext(ExprContext *ctx);

    antlr4::Token *op = nullptr;
    antlr4::tree::TerminalNode *LP();
    std::vector<ExprContext *> expr();
    ExprContext* expr(size_t i);
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  ExprContext* expr();


private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

