grammar HelixLispy;
import HelixLexerRules;

cu : (expr NL)+ EOF
   ;

expr : '(' 'IF' expr expr expr ')'                        #If
   | '(' op=('GE'|'LE'|'EQ'|'GT'|'LT'|'NE'|'AND'|'OR'|'MUL'|'ADD') expr expr ')' #BinaryOp
   | '(' op=('NEG'|'INV'|'NOT') expr ')'                  #UnaryOp
   | '(' 'BIND' ID expr ')'                               #Bind
   | '(' 'ID' ID ')'                                      #Id
   | '(' 'FLOAT' DOUBLE ')'                               #Float
   | '(' 'DOUBLE' DOUBLE ')'                              #Double
   | '(' 'INT' INT ')'                                    #Int
   | '(' 'BOOL' BOOL ')'                                  #Bool
   ;

