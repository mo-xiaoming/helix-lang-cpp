
// Generated from HelixLispy.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "HelixLispyVisitor.h"


/**
 * This class provides an empty implementation of HelixLispyVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  HelixLispyBaseVisitor : public HelixLispyVisitor {
public:

  virtual antlrcpp::Any visitCu(HelixLispyParser::CuContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIf(HelixLispyParser::IfContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinaryOp(HelixLispyParser::BinaryOpContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitUnaryOp(HelixLispyParser::UnaryOpContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBind(HelixLispyParser::BindContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitId(HelixLispyParser::IdContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFloat(HelixLispyParser::FloatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDouble(HelixLispyParser::DoubleContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInt(HelixLispyParser::IntContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBool(HelixLispyParser::BoolContext *ctx) override {
    return visitChildren(ctx);
  }


};

