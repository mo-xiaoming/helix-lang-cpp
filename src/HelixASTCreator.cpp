#include "HelixASTCreator.hpp"

#include "AST.hpp"

antlrcpp::Any HelixASTCreator::visitCu(HelixLispyParser::CuContext *ctx) {
  const auto ecs = ctx->expr();
  std::vector<CPtr<Expr>> es;
  es.reserve(ecs.size());
  for (auto *ec : ecs) {
    es.push_back(std::move(visit(ec).as<CPtr<Expr>>()));
  }
  return es;
}

antlrcpp::Any HelixASTCreator::visitIf(HelixLispyParser::IfContext *ctx) {
  CPtr<Expr> c = visit(ctx->expr(0)).as<CPtr<Expr>>();
  CPtr<Expr> t = visit(ctx->expr(1)).as<CPtr<Expr>>();
  CPtr<Expr> f = visit(ctx->expr(2)).as<CPtr<Expr>>();
  const CPtr<Expr> e = MkCPtr<IfExpr>(std::move(c), std::move(t), std::move(f));
  return e;
}

antlrcpp::Any HelixASTCreator::visitBind(HelixLispyParser::BindContext *ctx) {
  CPtr<Expr> a = visit(ctx->expr()).as<CPtr<Expr>>();
  const CPtr<Expr> e = MkCPtr<BindExpr>(ctx->ID()->getText(), std::move(a));
  return e;
}

antlrcpp::Any HelixASTCreator::visitId(HelixLispyParser::IdContext *ctx) {
  CPtr<Expr> e = MkCPtr<IdExpr>(ctx->ID()->getText());
  return e;
}

antlrcpp::Any
HelixASTCreator::visitBinaryOp(HelixLispyParser::BinaryOpContext *ctx) {
  CPtr<Expr> a0 = visit(ctx->expr(0)).as<CPtr<Expr>>();
  CPtr<Expr> a1 = visit(ctx->expr(1)).as<CPtr<Expr>>();
  CPtr<Expr> e;
  const std::string op = ctx->op->getText();
  if (op == "MUL") {
    e = MkCPtr<MulExpr>(std::move(a0), std::move(a1));
  } else if (op == "ADD") {
    e = MkCPtr<AddExpr>(std::move(a0), std::move(a1));
  } else if (op == "OR") {
    e = MkCPtr<OrExpr>(std::move(a0), std::move(a1));
  } else if (op == "AND") {
    e = MkCPtr<AndExpr>(std::move(a0), std::move(a1));
  } else if (op == "NE") {
    e = MkCPtr<NeExpr>(std::move(a0), std::move(a1));
  } else if (op == "LT") {
    e = MkCPtr<LtExpr>(std::move(a0), std::move(a1));
  } else if (op == "GT") {
    e = MkCPtr<GtExpr>(std::move(a0), std::move(a1));
  } else if (op == "EQ") {
    e = MkCPtr<EqExpr>(std::move(a0), std::move(a1));
  } else if (op == "LE") {
    e = MkCPtr<LeExpr>(std::move(a0), std::move(a1));
  } else if (op == "GE") {
    e = MkCPtr<GeExpr>(std::move(a0), std::move(a1));
  }
  assert(e); // NOLINT
  return e;
}

antlrcpp::Any
HelixASTCreator::visitUnaryOp(HelixLispyParser::UnaryOpContext *ctx) {
  CPtr<Expr> a = visit(ctx->expr()).as<CPtr<Expr>>();
  CPtr<Expr> e;
  const std::string op = ctx->op->getText();
  if (op == "NEG") {
    e = MkCPtr<NegExpr>(std::move(a));
  } else if (op == "INV") {
    e = MkCPtr<InvExpr>(std::move(a));
  } else if (op == "NOT") {
    e = MkCPtr<NotExpr>(std::move(a));
  }
  assert(e); // NOLINT
  return e;
}

antlrcpp::Any HelixASTCreator::visitFloat(HelixLispyParser::FloatContext *ctx) {
  const CPtr<Expr> e = MkCPtr<FloatExpr>(ctx->DOUBLE()->getText());
  return e;
}

antlrcpp::Any
HelixASTCreator::visitDouble(HelixLispyParser::DoubleContext *ctx) {
  const CPtr<Expr> e = MkCPtr<DoubleExpr>(ctx->DOUBLE()->getText());
  return e;
}

antlrcpp::Any HelixASTCreator::visitInt(HelixLispyParser::IntContext *ctx) {
  const CPtr<Expr> e = MkCPtr<IntExpr>(ctx->INT()->getText());
  return e;
}

antlrcpp::Any HelixASTCreator::visitBool(HelixLispyParser::BoolContext *ctx) {
  const CPtr<Expr> e = MkCPtr<BoolExpr>(ctx->BOOL()->getText());
  return e;
}
