antlr4 -Dlanguage=Cpp -no-listener -visitor Helix.g4
clang-tidy --quiet --fix --checks='-*,misc-unused-parameters' HelixParser.cpp
antlr4 -Dlanguage=Cpp -no-listener -visitor HelixLispy.g4
clang-tidy --quiet --fix --checks='-*,misc-unused-parameters' HelixLispyParser.cpp
