
// Generated from Helix.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "HelixVisitor.h"


/**
 * This class provides an empty implementation of HelixVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  HelixBaseVisitor : public HelixVisitor {
public:

  virtual antlrcpp::Any visitCu(HelixParser::CuContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExpr(HelixParser::ExprContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNl(HelixParser::NlContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBind(HelixParser::BindContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAddSub(HelixParser::AddSubContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDouble(HelixParser::DoubleContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitInt(HelixParser::IntContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFloat(HelixParser::FloatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIdentifier(HelixParser::IdentifierContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBool(HelixParser::BoolContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCompare(HelixParser::CompareContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinary(HelixParser::BinaryContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDivMul(HelixParser::DivMulContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIf(HelixParser::IfContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBinaryNeg(HelixParser::BinaryNegContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParen(HelixParser::ParenContext *ctx) override {
    return visitChildren(ctx);
  }


};

