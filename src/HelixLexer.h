
// Generated from Helix.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"




class  HelixLexer : public antlr4::Lexer {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, BIND = 4, MUL = 5, DIV = 6, ADD = 7, SUB = 8, 
    GE = 9, LE = 10, EQ = 11, GT = 12, LT = 13, NE = 14, NOT = 15, AND = 16, 
    OR = 17, LB = 18, RB = 19, LP = 20, RP = 21, BOOL = 22, ID = 23, FLOAT = 24, 
    INT = 25, DOUBLE = 26, STRING = 27, LINE_COMMENT = 28, COMMENT = 29, 
    NL = 30, WS = 31
  };

  HelixLexer(antlr4::CharStream *input);
  ~HelixLexer();

  virtual std::string getGrammarFileName() const override;
  virtual const std::vector<std::string>& getRuleNames() const override;

  virtual const std::vector<std::string>& getChannelNames() const override;
  virtual const std::vector<std::string>& getModeNames() const override;
  virtual const std::vector<std::string>& getTokenNames() const override; // deprecated, use vocabulary instead
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;

  virtual const std::vector<uint16_t> getSerializedATN() const override;
  virtual const antlr4::atn::ATN& getATN() const override;

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;
  static std::vector<std::string> _channelNames;
  static std::vector<std::string> _modeNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

