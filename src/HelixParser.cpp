
// Generated from Helix.g4 by ANTLR 4.8


#include "HelixVisitor.h"

#include "HelixParser.h"


using namespace antlrcpp;
using namespace antlr4;

HelixParser::HelixParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

HelixParser::~HelixParser() {
  delete _interpreter;
}

std::string HelixParser::getGrammarFileName() const {
  return "Helix.g4";
}

const std::vector<std::string>& HelixParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& HelixParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- CuContext ------------------------------------------------------------------

HelixParser::CuContext::CuContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* HelixParser::CuContext::EOF() {
  return getToken(HelixParser::EOF, 0);
}

std::vector<HelixParser::LineContext *> HelixParser::CuContext::line() {
  return getRuleContexts<HelixParser::LineContext>();
}

HelixParser::LineContext* HelixParser::CuContext::line(size_t i) {
  return getRuleContext<HelixParser::LineContext>(i);
}


size_t HelixParser::CuContext::getRuleIndex() const {
  return HelixParser::RuleCu;
}


antlrcpp::Any HelixParser::CuContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitCu(this);
  else
    return visitor->visitChildren(this);
}

HelixParser::CuContext* HelixParser::cu() {
  CuContext *_localctx = _tracker.createInstance<CuContext>(_ctx, getState());
  enterRule(_localctx, 0, HelixParser::RuleCu);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(9);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << HelixParser::T__0)
      | (1ULL << HelixParser::NOT)
      | (1ULL << HelixParser::LP)
      | (1ULL << HelixParser::BOOL)
      | (1ULL << HelixParser::ID)
      | (1ULL << HelixParser::FLOAT)
      | (1ULL << HelixParser::INT)
      | (1ULL << HelixParser::DOUBLE)
      | (1ULL << HelixParser::NL))) != 0)) {
      setState(6);
      line();
      setState(11);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(12);
    match(HelixParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LineContext ------------------------------------------------------------------

HelixParser::LineContext::LineContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t HelixParser::LineContext::getRuleIndex() const {
  return HelixParser::RuleLine;
}

void HelixParser::LineContext::copyFrom(LineContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- ExprContext ------------------------------------------------------------------

HelixParser::EContext* HelixParser::ExprContext::e() {
  return getRuleContext<HelixParser::EContext>(0);
}

HelixParser::ExprContext::ExprContext(LineContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::ExprContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitExpr(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NlContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::NlContext::NL() {
  return getToken(HelixParser::NL, 0);
}

HelixParser::NlContext::NlContext(LineContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::NlContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitNl(this);
  else
    return visitor->visitChildren(this);
}
HelixParser::LineContext* HelixParser::line() {
  LineContext *_localctx = _tracker.createInstance<LineContext>(_ctx, getState());
  enterRule(_localctx, 2, HelixParser::RuleLine);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(16);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case HelixParser::T__0:
      case HelixParser::NOT:
      case HelixParser::LP:
      case HelixParser::BOOL:
      case HelixParser::ID:
      case HelixParser::FLOAT:
      case HelixParser::INT:
      case HelixParser::DOUBLE: {
        _localctx = dynamic_cast<LineContext *>(_tracker.createInstance<HelixParser::ExprContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(14);
        e(0);
        break;
      }

      case HelixParser::NL: {
        _localctx = dynamic_cast<LineContext *>(_tracker.createInstance<HelixParser::NlContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(15);
        match(HelixParser::NL);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- EContext ------------------------------------------------------------------

HelixParser::EContext::EContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t HelixParser::EContext::getRuleIndex() const {
  return HelixParser::RuleE;
}

void HelixParser::EContext::copyFrom(EContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- BindContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::BindContext::ID() {
  return getToken(HelixParser::ID, 0);
}

tree::TerminalNode* HelixParser::BindContext::BIND() {
  return getToken(HelixParser::BIND, 0);
}

HelixParser::EContext* HelixParser::BindContext::e() {
  return getRuleContext<HelixParser::EContext>(0);
}

HelixParser::BindContext::BindContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::BindContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitBind(this);
  else
    return visitor->visitChildren(this);
}
//----------------- AddSubContext ------------------------------------------------------------------

std::vector<HelixParser::EContext *> HelixParser::AddSubContext::e() {
  return getRuleContexts<HelixParser::EContext>();
}

HelixParser::EContext* HelixParser::AddSubContext::e(size_t i) {
  return getRuleContext<HelixParser::EContext>(i);
}

tree::TerminalNode* HelixParser::AddSubContext::ADD() {
  return getToken(HelixParser::ADD, 0);
}

tree::TerminalNode* HelixParser::AddSubContext::SUB() {
  return getToken(HelixParser::SUB, 0);
}

HelixParser::AddSubContext::AddSubContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::AddSubContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitAddSub(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DoubleContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::DoubleContext::DOUBLE() {
  return getToken(HelixParser::DOUBLE, 0);
}

HelixParser::DoubleContext::DoubleContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::DoubleContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitDouble(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IntContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::IntContext::INT() {
  return getToken(HelixParser::INT, 0);
}

HelixParser::IntContext::IntContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::IntContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitInt(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FloatContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::FloatContext::FLOAT() {
  return getToken(HelixParser::FLOAT, 0);
}

HelixParser::FloatContext::FloatContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::FloatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitFloat(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IdentifierContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::IdentifierContext::ID() {
  return getToken(HelixParser::ID, 0);
}

HelixParser::IdentifierContext::IdentifierContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::IdentifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitIdentifier(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BoolContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::BoolContext::BOOL() {
  return getToken(HelixParser::BOOL, 0);
}

HelixParser::BoolContext::BoolContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::BoolContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitBool(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CompareContext ------------------------------------------------------------------

std::vector<HelixParser::EContext *> HelixParser::CompareContext::e() {
  return getRuleContexts<HelixParser::EContext>();
}

HelixParser::EContext* HelixParser::CompareContext::e(size_t i) {
  return getRuleContext<HelixParser::EContext>(i);
}

tree::TerminalNode* HelixParser::CompareContext::GE() {
  return getToken(HelixParser::GE, 0);
}

tree::TerminalNode* HelixParser::CompareContext::LE() {
  return getToken(HelixParser::LE, 0);
}

tree::TerminalNode* HelixParser::CompareContext::EQ() {
  return getToken(HelixParser::EQ, 0);
}

tree::TerminalNode* HelixParser::CompareContext::GT() {
  return getToken(HelixParser::GT, 0);
}

tree::TerminalNode* HelixParser::CompareContext::LT() {
  return getToken(HelixParser::LT, 0);
}

tree::TerminalNode* HelixParser::CompareContext::NE() {
  return getToken(HelixParser::NE, 0);
}

HelixParser::CompareContext::CompareContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::CompareContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitCompare(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BinaryContext ------------------------------------------------------------------

std::vector<HelixParser::EContext *> HelixParser::BinaryContext::e() {
  return getRuleContexts<HelixParser::EContext>();
}

HelixParser::EContext* HelixParser::BinaryContext::e(size_t i) {
  return getRuleContext<HelixParser::EContext>(i);
}

tree::TerminalNode* HelixParser::BinaryContext::AND() {
  return getToken(HelixParser::AND, 0);
}

tree::TerminalNode* HelixParser::BinaryContext::OR() {
  return getToken(HelixParser::OR, 0);
}

HelixParser::BinaryContext::BinaryContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::BinaryContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitBinary(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DivMulContext ------------------------------------------------------------------

std::vector<HelixParser::EContext *> HelixParser::DivMulContext::e() {
  return getRuleContexts<HelixParser::EContext>();
}

HelixParser::EContext* HelixParser::DivMulContext::e(size_t i) {
  return getRuleContext<HelixParser::EContext>(i);
}

tree::TerminalNode* HelixParser::DivMulContext::DIV() {
  return getToken(HelixParser::DIV, 0);
}

tree::TerminalNode* HelixParser::DivMulContext::MUL() {
  return getToken(HelixParser::MUL, 0);
}

HelixParser::DivMulContext::DivMulContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::DivMulContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitDivMul(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IfContext ------------------------------------------------------------------

std::vector<HelixParser::EContext *> HelixParser::IfContext::e() {
  return getRuleContexts<HelixParser::EContext>();
}

HelixParser::EContext* HelixParser::IfContext::e(size_t i) {
  return getRuleContext<HelixParser::EContext>(i);
}

HelixParser::IfContext::IfContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::IfContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitIf(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BinaryNegContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::BinaryNegContext::NOT() {
  return getToken(HelixParser::NOT, 0);
}

HelixParser::EContext* HelixParser::BinaryNegContext::e() {
  return getRuleContext<HelixParser::EContext>(0);
}

HelixParser::BinaryNegContext::BinaryNegContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::BinaryNegContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitBinaryNeg(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ParenContext ------------------------------------------------------------------

tree::TerminalNode* HelixParser::ParenContext::LP() {
  return getToken(HelixParser::LP, 0);
}

HelixParser::EContext* HelixParser::ParenContext::e() {
  return getRuleContext<HelixParser::EContext>(0);
}

tree::TerminalNode* HelixParser::ParenContext::RP() {
  return getToken(HelixParser::RP, 0);
}

HelixParser::ParenContext::ParenContext(EContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixParser::ParenContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixVisitor*>(visitor))
    return parserVisitor->visitParen(this);
  else
    return visitor->visitChildren(this);
}

HelixParser::EContext* HelixParser::e() {
   return e(0);
}

HelixParser::EContext* HelixParser::e(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  HelixParser::EContext *_localctx = _tracker.createInstance<EContext>(_ctx, parentState);
  HelixParser::EContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 4;
  enterRecursionRule(_localctx, 4, HelixParser::RuleE, precedence);

    size_t _la = 0;

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(40);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx)) {
    case 1: {
      _localctx = _tracker.createInstance<IfContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;

      setState(19);
      match(HelixParser::T__0);
      setState(20);
      e(0);
      setState(21);
      match(HelixParser::T__1);
      setState(22);
      e(0);
      setState(23);
      match(HelixParser::T__2);
      setState(24);
      e(13);
      break;
    }

    case 2: {
      _localctx = _tracker.createInstance<BinaryNegContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(26);
      match(HelixParser::NOT);
      setState(27);
      e(10);
      break;
    }

    case 3: {
      _localctx = _tracker.createInstance<BindContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(28);
      match(HelixParser::ID);
      setState(29);
      match(HelixParser::BIND);
      setState(30);
      e(7);
      break;
    }

    case 4: {
      _localctx = _tracker.createInstance<ParenContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(31);
      match(HelixParser::LP);
      setState(32);
      e(0);
      setState(33);
      match(HelixParser::RP);
      break;
    }

    case 5: {
      _localctx = _tracker.createInstance<IdentifierContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(35);
      match(HelixParser::ID);
      break;
    }

    case 6: {
      _localctx = _tracker.createInstance<FloatContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(36);
      match(HelixParser::FLOAT);
      break;
    }

    case 7: {
      _localctx = _tracker.createInstance<DoubleContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(37);
      match(HelixParser::DOUBLE);
      break;
    }

    case 8: {
      _localctx = _tracker.createInstance<IntContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(38);
      match(HelixParser::INT);
      break;
    }

    case 9: {
      _localctx = _tracker.createInstance<BoolContext>(_localctx);
      _ctx = _localctx;
      previousContext = _localctx;
      setState(39);
      match(HelixParser::BOOL);
      break;
    }

    }
    _ctx->stop = _input->LT(-1);
    setState(56);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(54);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx)) {
        case 1: {
          auto newContext = _tracker.createInstance<CompareContext>(_tracker.createInstance<EContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleE);
          setState(42);

          if (!(precpred(_ctx, 12))) throw FailedPredicateException(this, "precpred(_ctx, 12)");
          setState(43);
          dynamic_cast<CompareContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!((((_la & ~ 0x3fULL) == 0) &&
            ((1ULL << _la) & ((1ULL << HelixParser::GE)
            | (1ULL << HelixParser::LE)
            | (1ULL << HelixParser::EQ)
            | (1ULL << HelixParser::GT)
            | (1ULL << HelixParser::LT)
            | (1ULL << HelixParser::NE))) != 0))) {
            dynamic_cast<CompareContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(44);
          e(13);
          break;
        }

        case 2: {
          auto newContext = _tracker.createInstance<BinaryContext>(_tracker.createInstance<EContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleE);
          setState(45);

          if (!(precpred(_ctx, 11))) throw FailedPredicateException(this, "precpred(_ctx, 11)");
          setState(46);
          dynamic_cast<BinaryContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == HelixParser::AND

          || _la == HelixParser::OR)) {
            dynamic_cast<BinaryContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(47);
          e(12);
          break;
        }

        case 3: {
          auto newContext = _tracker.createInstance<DivMulContext>(_tracker.createInstance<EContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleE);
          setState(48);

          if (!(precpred(_ctx, 9))) throw FailedPredicateException(this, "precpred(_ctx, 9)");
          setState(49);
          dynamic_cast<DivMulContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == HelixParser::MUL

          || _la == HelixParser::DIV)) {
            dynamic_cast<DivMulContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(50);
          e(10);
          break;
        }

        case 4: {
          auto newContext = _tracker.createInstance<AddSubContext>(_tracker.createInstance<EContext>(parentContext, parentState));
          _localctx = newContext;
          pushNewRecursionContext(newContext, startState, RuleE);
          setState(51);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(52);
          dynamic_cast<AddSubContext *>(_localctx)->op = _input->LT(1);
          _la = _input->LA(1);
          if (!(_la == HelixParser::ADD

          || _la == HelixParser::SUB)) {
            dynamic_cast<AddSubContext *>(_localctx)->op = _errHandler->recoverInline(this);
          }
          else {
            _errHandler->reportMatch(this);
            consume();
          }
          setState(53);
          e(9);
          break;
        }

        } 
      }
      setState(58);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

bool HelixParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 2: return eSempred(dynamic_cast<EContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool HelixParser::eSempred(EContext * /*_localctx*/, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 12);
    case 1: return precpred(_ctx, 11);
    case 2: return precpred(_ctx, 9);
    case 3: return precpred(_ctx, 8);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> HelixParser::_decisionToDFA;
atn::PredictionContextCache HelixParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN HelixParser::_atn;
std::vector<uint16_t> HelixParser::_serializedATN;

std::vector<std::string> HelixParser::_ruleNames = {
  "cu", "line", "e"
};

std::vector<std::string> HelixParser::_literalNames = {
  "", "'if'", "'then'", "'else'", "'='", "'*'", "'/'", "'+'", "'-'", "'ge'", 
  "'le'", "'eq'", "'gt'", "'lt'", "'ne'", "'not'", "'and'", "'or'", "'{'", 
  "'}'", "'('", "')'"
};

std::vector<std::string> HelixParser::_symbolicNames = {
  "", "", "", "", "BIND", "MUL", "DIV", "ADD", "SUB", "GE", "LE", "EQ", 
  "GT", "LT", "NE", "NOT", "AND", "OR", "LB", "RB", "LP", "RP", "BOOL", 
  "ID", "FLOAT", "INT", "DOUBLE", "STRING", "LINE_COMMENT", "COMMENT", "NL", 
  "WS"
};

dfa::Vocabulary HelixParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> HelixParser::_tokenNames;

HelixParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x21, 0x3e, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x3, 0x2, 0x7, 0x2, 0xa, 0xa, 0x2, 0xc, 0x2, 0xe, 0x2, 0xd, 0xb, 
    0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x5, 0x3, 0x13, 0xa, 0x3, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 
    0x3, 0x4, 0x5, 0x4, 0x2b, 0xa, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x7, 0x4, 0x39, 0xa, 0x4, 0xc, 0x4, 0xe, 0x4, 0x3c, 0xb, 
    0x4, 0x3, 0x4, 0x2, 0x3, 0x6, 0x5, 0x2, 0x4, 0x6, 0x2, 0x6, 0x3, 0x2, 
    0xb, 0x10, 0x3, 0x2, 0x12, 0x13, 0x3, 0x2, 0x7, 0x8, 0x3, 0x2, 0x9, 
    0xa, 0x2, 0x48, 0x2, 0xb, 0x3, 0x2, 0x2, 0x2, 0x4, 0x12, 0x3, 0x2, 0x2, 
    0x2, 0x6, 0x2a, 0x3, 0x2, 0x2, 0x2, 0x8, 0xa, 0x5, 0x4, 0x3, 0x2, 0x9, 
    0x8, 0x3, 0x2, 0x2, 0x2, 0xa, 0xd, 0x3, 0x2, 0x2, 0x2, 0xb, 0x9, 0x3, 
    0x2, 0x2, 0x2, 0xb, 0xc, 0x3, 0x2, 0x2, 0x2, 0xc, 0xe, 0x3, 0x2, 0x2, 
    0x2, 0xd, 0xb, 0x3, 0x2, 0x2, 0x2, 0xe, 0xf, 0x7, 0x2, 0x2, 0x3, 0xf, 
    0x3, 0x3, 0x2, 0x2, 0x2, 0x10, 0x13, 0x5, 0x6, 0x4, 0x2, 0x11, 0x13, 
    0x7, 0x20, 0x2, 0x2, 0x12, 0x10, 0x3, 0x2, 0x2, 0x2, 0x12, 0x11, 0x3, 
    0x2, 0x2, 0x2, 0x13, 0x5, 0x3, 0x2, 0x2, 0x2, 0x14, 0x15, 0x8, 0x4, 
    0x1, 0x2, 0x15, 0x16, 0x7, 0x3, 0x2, 0x2, 0x16, 0x17, 0x5, 0x6, 0x4, 
    0x2, 0x17, 0x18, 0x7, 0x4, 0x2, 0x2, 0x18, 0x19, 0x5, 0x6, 0x4, 0x2, 
    0x19, 0x1a, 0x7, 0x5, 0x2, 0x2, 0x1a, 0x1b, 0x5, 0x6, 0x4, 0xf, 0x1b, 
    0x2b, 0x3, 0x2, 0x2, 0x2, 0x1c, 0x1d, 0x7, 0x11, 0x2, 0x2, 0x1d, 0x2b, 
    0x5, 0x6, 0x4, 0xc, 0x1e, 0x1f, 0x7, 0x19, 0x2, 0x2, 0x1f, 0x20, 0x7, 
    0x6, 0x2, 0x2, 0x20, 0x2b, 0x5, 0x6, 0x4, 0x9, 0x21, 0x22, 0x7, 0x16, 
    0x2, 0x2, 0x22, 0x23, 0x5, 0x6, 0x4, 0x2, 0x23, 0x24, 0x7, 0x17, 0x2, 
    0x2, 0x24, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x25, 0x2b, 0x7, 0x19, 0x2, 0x2, 
    0x26, 0x2b, 0x7, 0x1a, 0x2, 0x2, 0x27, 0x2b, 0x7, 0x1c, 0x2, 0x2, 0x28, 
    0x2b, 0x7, 0x1b, 0x2, 0x2, 0x29, 0x2b, 0x7, 0x18, 0x2, 0x2, 0x2a, 0x14, 
    0x3, 0x2, 0x2, 0x2, 0x2a, 0x1c, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x1e, 0x3, 
    0x2, 0x2, 0x2, 0x2a, 0x21, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x25, 0x3, 0x2, 
    0x2, 0x2, 0x2a, 0x26, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x27, 0x3, 0x2, 0x2, 
    0x2, 0x2a, 0x28, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x29, 0x3, 0x2, 0x2, 0x2, 
    0x2b, 0x3a, 0x3, 0x2, 0x2, 0x2, 0x2c, 0x2d, 0xc, 0xe, 0x2, 0x2, 0x2d, 
    0x2e, 0x9, 0x2, 0x2, 0x2, 0x2e, 0x39, 0x5, 0x6, 0x4, 0xf, 0x2f, 0x30, 
    0xc, 0xd, 0x2, 0x2, 0x30, 0x31, 0x9, 0x3, 0x2, 0x2, 0x31, 0x39, 0x5, 
    0x6, 0x4, 0xe, 0x32, 0x33, 0xc, 0xb, 0x2, 0x2, 0x33, 0x34, 0x9, 0x4, 
    0x2, 0x2, 0x34, 0x39, 0x5, 0x6, 0x4, 0xc, 0x35, 0x36, 0xc, 0xa, 0x2, 
    0x2, 0x36, 0x37, 0x9, 0x5, 0x2, 0x2, 0x37, 0x39, 0x5, 0x6, 0x4, 0xb, 
    0x38, 0x2c, 0x3, 0x2, 0x2, 0x2, 0x38, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x38, 
    0x32, 0x3, 0x2, 0x2, 0x2, 0x38, 0x35, 0x3, 0x2, 0x2, 0x2, 0x39, 0x3c, 
    0x3, 0x2, 0x2, 0x2, 0x3a, 0x38, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x3b, 0x3, 
    0x2, 0x2, 0x2, 0x3b, 0x7, 0x3, 0x2, 0x2, 0x2, 0x3c, 0x3a, 0x3, 0x2, 
    0x2, 0x2, 0x7, 0xb, 0x12, 0x2a, 0x38, 0x3a, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

HelixParser::Initializer HelixParser::_init;
