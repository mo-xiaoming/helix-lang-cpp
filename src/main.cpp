#include "AST.hpp"
#include "HelixASTCreator.hpp"
#include "HelixLexer.h"
#include "HelixLispyBaseVisitor.h"
#include "HelixLispyCreator.hpp"
#include "HelixLispyLexer.h"
#include "HelixLispyParser.h"
#include "HelixParser.h"
#include "HelixUnderlineErrorListener.hpp"

#include <spdlog/spdlog.h>

#include <any>
#include <string>

using namespace std::literals;

namespace {
template <typename Ret, typename Input, typename Lexer, typename Parser,
          typename Visitor>
[[nodiscard]] Ret Transform(Input &in) {
  antlr4::ANTLRInputStream input(in);
  Lexer lexer(&input);
  antlr4::CommonTokenStream tokens(&lexer);
  Parser parser(&tokens);
  parser.removeErrorListeners();
  UnderlineErrorListener errorListner;
  parser.addErrorListener(&errorListner);
  parser.template getInterpreter<antlr4::atn::ParserATNSimulator>()
      ->setPredictionMode(
          antlr4::atn::PredictionMode::LL_EXACT_AMBIG_DETECTION);
  typename Parser::CuContext *tree = parser.cu();

  Visitor visitor;
  const antlrcpp::Any r = visitor.visit(tree);
  return r.as<Ret>();
}

std::string
stringifyLisps(const std::vector<std::pair<std::string, std::string>> &lisps) {
  auto buf = fmt::memory_buffer();
  for (const auto &l : lisps) {
    fmt::format_to(std::back_inserter(buf), FMT_STRING("{}\n"), l.second);
  }
  return std::string(buf.data(), buf.size());
}

void typeCheck(const std::vector<CPtr<Expr>> &es) {
  TypeCheckVisitor v;
  for (const auto &e : es) {
    fmt::print(FMT_STRING("{}\n"), e->toString());
    const auto t = e->accept(v);
    if (const auto *ts = std::any_cast<CPtr<PrimType>>(&t); ts != nullptr) {
      fmt::print(FMT_STRING("  -> {}\n"), (*ts)->toString());
    } else if (const auto *te = std::any_cast<decltype(v)::Error>(&t);
               te != nullptr) {
      fmt::print(FMT_STRING("  -> ERROR: {}\n"), te->what());
    } else {
      fmt::print(FMT_STRING("  -> something wrong\n"));
    }
  }
}

void eval(const std::vector<CPtr<Expr>> &es) {
  EvalVisitor ev;
  for (const auto &e : es) {
    fmt::print(FMT_STRING("{}\n"), e->toString());
    const auto t = e->accept(ev);
    if (const auto *ts = std::any_cast<EvalVisitor::EvalResult>(&t);
        ts != nullptr) {
      fmt::print(
          FMT_STRING("  -> {}::{}\n"),
          std::visit([](auto i) { return std::to_string(i); }, ts->nativeValue),
          ts->primType->toString());
    } else if (const auto *te = std::any_cast<decltype(ev)::Error>(&t);
               te != nullptr) {
      fmt::print(FMT_STRING("  -> ERROR: {}\n"), te->what());
    } else {
      fmt::print(FMT_STRING("  -> something wrong\n"));
    }
  }
}

std::string badge(const std::string &s, int width = 20) {
  return fmt::format(FMT_STRING("┌{0:─^{2}}┐\n"
                                "│{1: ^{2}}│\n"
                                "└{0:─^{2}}┘\n"),
                     "", s, width);
}
} // namespace

int main(int argc, char **argv) {
  if (argc != 2) {
    fmt::print(stderr, FMT_STRING("{}\n"), "missing hlx filename");
    return 1;
  }

  std::ios_base::sync_with_stdio(false);

  std::ifstream inf(argv[1]);
  const auto lispy =
      Transform<std::vector<std::pair<std::string, std::string>>, std::ifstream,
                HelixLexer, HelixParser, HelixLispyCreator>(inf);
  const auto lisps = stringifyLisps(lispy);
  const auto ast =
      Transform<std::vector<CPtr<Expr>>, decltype(lisps), HelixLispyLexer,
                HelixLispyParser, HelixASTCreator>(lisps);

  fmt::print(FMT_STRING("{}\n"), badge("type checking"));
  typeCheck(ast);
  fmt::print(FMT_STRING("{}\n"), badge("evaluation"));
  eval(ast);
}
