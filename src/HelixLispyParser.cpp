
// Generated from HelixLispy.g4 by ANTLR 4.8


#include "HelixLispyVisitor.h"

#include "HelixLispyParser.h"


using namespace antlrcpp;
using namespace antlr4;

HelixLispyParser::HelixLispyParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

HelixLispyParser::~HelixLispyParser() {
  delete _interpreter;
}

std::string HelixLispyParser::getGrammarFileName() const {
  return "HelixLispy.g4";
}

const std::vector<std::string>& HelixLispyParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& HelixLispyParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- CuContext ------------------------------------------------------------------

HelixLispyParser::CuContext::CuContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* HelixLispyParser::CuContext::EOF() {
  return getToken(HelixLispyParser::EOF, 0);
}

std::vector<HelixLispyParser::ExprContext *> HelixLispyParser::CuContext::expr() {
  return getRuleContexts<HelixLispyParser::ExprContext>();
}

HelixLispyParser::ExprContext* HelixLispyParser::CuContext::expr(size_t i) {
  return getRuleContext<HelixLispyParser::ExprContext>(i);
}

std::vector<tree::TerminalNode *> HelixLispyParser::CuContext::NL() {
  return getTokens(HelixLispyParser::NL);
}

tree::TerminalNode* HelixLispyParser::CuContext::NL(size_t i) {
  return getToken(HelixLispyParser::NL, i);
}


size_t HelixLispyParser::CuContext::getRuleIndex() const {
  return HelixLispyParser::RuleCu;
}


antlrcpp::Any HelixLispyParser::CuContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitCu(this);
  else
    return visitor->visitChildren(this);
}

HelixLispyParser::CuContext* HelixLispyParser::cu() {
  CuContext *_localctx = _tracker.createInstance<CuContext>(_ctx, getState());
  enterRule(_localctx, 0, HelixLispyParser::RuleCu);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(7); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(4);
      expr();
      setState(5);
      match(HelixLispyParser::NL);
      setState(9); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while (_la == HelixLispyParser::LP);
    setState(11);
    match(HelixLispyParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExprContext ------------------------------------------------------------------

HelixLispyParser::ExprContext::ExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t HelixLispyParser::ExprContext::getRuleIndex() const {
  return HelixLispyParser::RuleExpr;
}

void HelixLispyParser::ExprContext::copyFrom(ExprContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- FloatContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::FloatContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

tree::TerminalNode* HelixLispyParser::FloatContext::DOUBLE() {
  return getToken(HelixLispyParser::DOUBLE, 0);
}

tree::TerminalNode* HelixLispyParser::FloatContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::FloatContext::FloatContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::FloatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitFloat(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BindContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::BindContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

tree::TerminalNode* HelixLispyParser::BindContext::ID() {
  return getToken(HelixLispyParser::ID, 0);
}

HelixLispyParser::ExprContext* HelixLispyParser::BindContext::expr() {
  return getRuleContext<HelixLispyParser::ExprContext>(0);
}

tree::TerminalNode* HelixLispyParser::BindContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::BindContext::BindContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::BindContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitBind(this);
  else
    return visitor->visitChildren(this);
}
//----------------- UnaryOpContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::UnaryOpContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

HelixLispyParser::ExprContext* HelixLispyParser::UnaryOpContext::expr() {
  return getRuleContext<HelixLispyParser::ExprContext>(0);
}

tree::TerminalNode* HelixLispyParser::UnaryOpContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::UnaryOpContext::UnaryOpContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::UnaryOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitUnaryOp(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BoolContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::BoolContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

tree::TerminalNode* HelixLispyParser::BoolContext::BOOL() {
  return getToken(HelixLispyParser::BOOL, 0);
}

tree::TerminalNode* HelixLispyParser::BoolContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::BoolContext::BoolContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::BoolContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitBool(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IdContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::IdContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

tree::TerminalNode* HelixLispyParser::IdContext::ID() {
  return getToken(HelixLispyParser::ID, 0);
}

tree::TerminalNode* HelixLispyParser::IdContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::IdContext::IdContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::IdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitId(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IfContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::IfContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

std::vector<HelixLispyParser::ExprContext *> HelixLispyParser::IfContext::expr() {
  return getRuleContexts<HelixLispyParser::ExprContext>();
}

HelixLispyParser::ExprContext* HelixLispyParser::IfContext::expr(size_t i) {
  return getRuleContext<HelixLispyParser::ExprContext>(i);
}

tree::TerminalNode* HelixLispyParser::IfContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::IfContext::IfContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::IfContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitIf(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DoubleContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::DoubleContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

tree::TerminalNode* HelixLispyParser::DoubleContext::DOUBLE() {
  return getToken(HelixLispyParser::DOUBLE, 0);
}

tree::TerminalNode* HelixLispyParser::DoubleContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::DoubleContext::DoubleContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::DoubleContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitDouble(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IntContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::IntContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

tree::TerminalNode* HelixLispyParser::IntContext::INT() {
  return getToken(HelixLispyParser::INT, 0);
}

tree::TerminalNode* HelixLispyParser::IntContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::IntContext::IntContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::IntContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitInt(this);
  else
    return visitor->visitChildren(this);
}
//----------------- BinaryOpContext ------------------------------------------------------------------

tree::TerminalNode* HelixLispyParser::BinaryOpContext::LP() {
  return getToken(HelixLispyParser::LP, 0);
}

std::vector<HelixLispyParser::ExprContext *> HelixLispyParser::BinaryOpContext::expr() {
  return getRuleContexts<HelixLispyParser::ExprContext>();
}

HelixLispyParser::ExprContext* HelixLispyParser::BinaryOpContext::expr(size_t i) {
  return getRuleContext<HelixLispyParser::ExprContext>(i);
}

tree::TerminalNode* HelixLispyParser::BinaryOpContext::RP() {
  return getToken(HelixLispyParser::RP, 0);
}

HelixLispyParser::BinaryOpContext::BinaryOpContext(ExprContext *ctx) { copyFrom(ctx); }


antlrcpp::Any HelixLispyParser::BinaryOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<HelixLispyVisitor*>(visitor))
    return parserVisitor->visitBinaryOp(this);
  else
    return visitor->visitChildren(this);
}
HelixLispyParser::ExprContext* HelixLispyParser::expr() {
  ExprContext *_localctx = _tracker.createInstance<ExprContext>(_ctx, getState());
  enterRule(_localctx, 2, HelixLispyParser::RuleExpr);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(57);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::IfContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(13);
      match(HelixLispyParser::LP);
      setState(14);
      match(HelixLispyParser::T__0);
      setState(15);
      expr();
      setState(16);
      expr();
      setState(17);
      expr();
      setState(18);
      match(HelixLispyParser::RP);
      break;
    }

    case 2: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::BinaryOpContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(20);
      match(HelixLispyParser::LP);
      setState(21);
      dynamic_cast<BinaryOpContext *>(_localctx)->op = _input->LT(1);
      _la = _input->LA(1);
      if (!((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << HelixLispyParser::T__1)
        | (1ULL << HelixLispyParser::T__2)
        | (1ULL << HelixLispyParser::T__3)
        | (1ULL << HelixLispyParser::T__4)
        | (1ULL << HelixLispyParser::T__5)
        | (1ULL << HelixLispyParser::T__6)
        | (1ULL << HelixLispyParser::T__7)
        | (1ULL << HelixLispyParser::T__8)
        | (1ULL << HelixLispyParser::T__9)
        | (1ULL << HelixLispyParser::T__10))) != 0))) {
        dynamic_cast<BinaryOpContext *>(_localctx)->op = _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(22);
      expr();
      setState(23);
      expr();
      setState(24);
      match(HelixLispyParser::RP);
      break;
    }

    case 3: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::UnaryOpContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(26);
      match(HelixLispyParser::LP);
      setState(27);
      dynamic_cast<UnaryOpContext *>(_localctx)->op = _input->LT(1);
      _la = _input->LA(1);
      if (!((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << HelixLispyParser::T__11)
        | (1ULL << HelixLispyParser::T__12)
        | (1ULL << HelixLispyParser::T__13))) != 0))) {
        dynamic_cast<UnaryOpContext *>(_localctx)->op = _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(28);
      expr();
      setState(29);
      match(HelixLispyParser::RP);
      break;
    }

    case 4: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::BindContext>(_localctx));
      enterOuterAlt(_localctx, 4);
      setState(31);
      match(HelixLispyParser::LP);
      setState(32);
      match(HelixLispyParser::T__14);
      setState(33);
      match(HelixLispyParser::ID);
      setState(34);
      expr();
      setState(35);
      match(HelixLispyParser::RP);
      break;
    }

    case 5: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::IdContext>(_localctx));
      enterOuterAlt(_localctx, 5);
      setState(37);
      match(HelixLispyParser::LP);
      setState(38);
      match(HelixLispyParser::T__15);
      setState(39);
      match(HelixLispyParser::ID);
      setState(40);
      match(HelixLispyParser::RP);
      break;
    }

    case 6: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::FloatContext>(_localctx));
      enterOuterAlt(_localctx, 6);
      setState(41);
      match(HelixLispyParser::LP);
      setState(42);
      match(HelixLispyParser::T__16);
      setState(43);
      match(HelixLispyParser::DOUBLE);
      setState(44);
      match(HelixLispyParser::RP);
      break;
    }

    case 7: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::DoubleContext>(_localctx));
      enterOuterAlt(_localctx, 7);
      setState(45);
      match(HelixLispyParser::LP);
      setState(46);
      match(HelixLispyParser::T__17);
      setState(47);
      match(HelixLispyParser::DOUBLE);
      setState(48);
      match(HelixLispyParser::RP);
      break;
    }

    case 8: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::IntContext>(_localctx));
      enterOuterAlt(_localctx, 8);
      setState(49);
      match(HelixLispyParser::LP);
      setState(50);
      match(HelixLispyParser::T__18);
      setState(51);
      match(HelixLispyParser::INT);
      setState(52);
      match(HelixLispyParser::RP);
      break;
    }

    case 9: {
      _localctx = dynamic_cast<ExprContext *>(_tracker.createInstance<HelixLispyParser::BoolContext>(_localctx));
      enterOuterAlt(_localctx, 9);
      setState(53);
      match(HelixLispyParser::LP);
      setState(54);
      match(HelixLispyParser::T__19);
      setState(55);
      match(HelixLispyParser::BOOL);
      setState(56);
      match(HelixLispyParser::RP);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> HelixLispyParser::_decisionToDFA;
atn::PredictionContextCache HelixLispyParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN HelixLispyParser::_atn;
std::vector<uint16_t> HelixLispyParser::_serializedATN;

std::vector<std::string> HelixLispyParser::_ruleNames = {
  "cu", "expr"
};

std::vector<std::string> HelixLispyParser::_literalNames = {
  "", "'IF'", "'GE'", "'LE'", "'EQ'", "'GT'", "'LT'", "'NE'", "'AND'", "'OR'", 
  "'MUL'", "'ADD'", "'NEG'", "'INV'", "'NOT'", "'BIND'", "'ID'", "'FLOAT'", 
  "'DOUBLE'", "'INT'", "'BOOL'", "'='", "'*'", "'/'", "'+'", "'-'", "'ge'", 
  "'le'", "'eq'", "'gt'", "'lt'", "'ne'", "'not'", "'and'", "'or'", "'{'", 
  "'}'", "'('", "')'"
};

std::vector<std::string> HelixLispyParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "BIND", "MUL", "DIV", "ADD", "SUB", "GE", "LE", "EQ", "GT", 
  "LT", "NE", "NOT", "AND", "OR", "LB", "RB", "LP", "RP", "BOOL", "ID", 
  "FLOAT", "INT", "DOUBLE", "STRING", "LINE_COMMENT", "COMMENT", "NL", "WS"
};

dfa::Vocabulary HelixLispyParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> HelixLispyParser::_tokenNames;

HelixLispyParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x32, 0x3e, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x3, 0x2, 0x3, 
    0x2, 0x3, 0x2, 0x6, 0x2, 0xa, 0xa, 0x2, 0xd, 0x2, 0xe, 0x2, 0xb, 0x3, 
    0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x5, 0x3, 0x3c, 0xa, 0x3, 0x3, 0x3, 
    0x2, 0x2, 0x4, 0x2, 0x4, 0x2, 0x4, 0x3, 0x2, 0x4, 0xd, 0x3, 0x2, 0xe, 
    0x10, 0x2, 0x44, 0x2, 0x9, 0x3, 0x2, 0x2, 0x2, 0x4, 0x3b, 0x3, 0x2, 
    0x2, 0x2, 0x6, 0x7, 0x5, 0x4, 0x3, 0x2, 0x7, 0x8, 0x7, 0x31, 0x2, 0x2, 
    0x8, 0xa, 0x3, 0x2, 0x2, 0x2, 0x9, 0x6, 0x3, 0x2, 0x2, 0x2, 0xa, 0xb, 
    0x3, 0x2, 0x2, 0x2, 0xb, 0x9, 0x3, 0x2, 0x2, 0x2, 0xb, 0xc, 0x3, 0x2, 
    0x2, 0x2, 0xc, 0xd, 0x3, 0x2, 0x2, 0x2, 0xd, 0xe, 0x7, 0x2, 0x2, 0x3, 
    0xe, 0x3, 0x3, 0x2, 0x2, 0x2, 0xf, 0x10, 0x7, 0x27, 0x2, 0x2, 0x10, 
    0x11, 0x7, 0x3, 0x2, 0x2, 0x11, 0x12, 0x5, 0x4, 0x3, 0x2, 0x12, 0x13, 
    0x5, 0x4, 0x3, 0x2, 0x13, 0x14, 0x5, 0x4, 0x3, 0x2, 0x14, 0x15, 0x7, 
    0x28, 0x2, 0x2, 0x15, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x16, 0x17, 0x7, 0x27, 
    0x2, 0x2, 0x17, 0x18, 0x9, 0x2, 0x2, 0x2, 0x18, 0x19, 0x5, 0x4, 0x3, 
    0x2, 0x19, 0x1a, 0x5, 0x4, 0x3, 0x2, 0x1a, 0x1b, 0x7, 0x28, 0x2, 0x2, 
    0x1b, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x1c, 0x1d, 0x7, 0x27, 0x2, 0x2, 0x1d, 
    0x1e, 0x9, 0x3, 0x2, 0x2, 0x1e, 0x1f, 0x5, 0x4, 0x3, 0x2, 0x1f, 0x20, 
    0x7, 0x28, 0x2, 0x2, 0x20, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x21, 0x22, 0x7, 
    0x27, 0x2, 0x2, 0x22, 0x23, 0x7, 0x11, 0x2, 0x2, 0x23, 0x24, 0x7, 0x2a, 
    0x2, 0x2, 0x24, 0x25, 0x5, 0x4, 0x3, 0x2, 0x25, 0x26, 0x7, 0x28, 0x2, 
    0x2, 0x26, 0x3c, 0x3, 0x2, 0x2, 0x2, 0x27, 0x28, 0x7, 0x27, 0x2, 0x2, 
    0x28, 0x29, 0x7, 0x12, 0x2, 0x2, 0x29, 0x2a, 0x7, 0x2a, 0x2, 0x2, 0x2a, 
    0x3c, 0x7, 0x28, 0x2, 0x2, 0x2b, 0x2c, 0x7, 0x27, 0x2, 0x2, 0x2c, 0x2d, 
    0x7, 0x13, 0x2, 0x2, 0x2d, 0x2e, 0x7, 0x2d, 0x2, 0x2, 0x2e, 0x3c, 0x7, 
    0x28, 0x2, 0x2, 0x2f, 0x30, 0x7, 0x27, 0x2, 0x2, 0x30, 0x31, 0x7, 0x14, 
    0x2, 0x2, 0x31, 0x32, 0x7, 0x2d, 0x2, 0x2, 0x32, 0x3c, 0x7, 0x28, 0x2, 
    0x2, 0x33, 0x34, 0x7, 0x27, 0x2, 0x2, 0x34, 0x35, 0x7, 0x15, 0x2, 0x2, 
    0x35, 0x36, 0x7, 0x2c, 0x2, 0x2, 0x36, 0x3c, 0x7, 0x28, 0x2, 0x2, 0x37, 
    0x38, 0x7, 0x27, 0x2, 0x2, 0x38, 0x39, 0x7, 0x16, 0x2, 0x2, 0x39, 0x3a, 
    0x7, 0x29, 0x2, 0x2, 0x3a, 0x3c, 0x7, 0x28, 0x2, 0x2, 0x3b, 0xf, 0x3, 
    0x2, 0x2, 0x2, 0x3b, 0x16, 0x3, 0x2, 0x2, 0x2, 0x3b, 0x1c, 0x3, 0x2, 
    0x2, 0x2, 0x3b, 0x21, 0x3, 0x2, 0x2, 0x2, 0x3b, 0x27, 0x3, 0x2, 0x2, 
    0x2, 0x3b, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x3b, 0x2f, 0x3, 0x2, 0x2, 0x2, 
    0x3b, 0x33, 0x3, 0x2, 0x2, 0x2, 0x3b, 0x37, 0x3, 0x2, 0x2, 0x2, 0x3c, 
    0x5, 0x3, 0x2, 0x2, 0x2, 0x4, 0xb, 0x3b, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

HelixLispyParser::Initializer HelixLispyParser::_init;
