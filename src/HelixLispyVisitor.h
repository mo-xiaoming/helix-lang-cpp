
// Generated from HelixLispy.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "HelixLispyParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by HelixLispyParser.
 */
class  HelixLispyVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by HelixLispyParser.
   */
    virtual antlrcpp::Any visitCu(HelixLispyParser::CuContext *context) = 0;

    virtual antlrcpp::Any visitIf(HelixLispyParser::IfContext *context) = 0;

    virtual antlrcpp::Any visitBinaryOp(HelixLispyParser::BinaryOpContext *context) = 0;

    virtual antlrcpp::Any visitUnaryOp(HelixLispyParser::UnaryOpContext *context) = 0;

    virtual antlrcpp::Any visitBind(HelixLispyParser::BindContext *context) = 0;

    virtual antlrcpp::Any visitId(HelixLispyParser::IdContext *context) = 0;

    virtual antlrcpp::Any visitFloat(HelixLispyParser::FloatContext *context) = 0;

    virtual antlrcpp::Any visitDouble(HelixLispyParser::DoubleContext *context) = 0;

    virtual antlrcpp::Any visitInt(HelixLispyParser::IntContext *context) = 0;

    virtual antlrcpp::Any visitBool(HelixLispyParser::BoolContext *context) = 0;


};

