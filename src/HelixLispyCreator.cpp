#include "HelixLispyCreator.hpp"

#include <fmt/format.h>

#include <string_view>

antlrcpp::Any HelixLispyCreator::visitCu(HelixParser::CuContext *ctx) {
  std::vector<std::pair<std::string, std::string>> lisps;
  for (auto *c : ctx->children) {
    antlrcpp::Any r = c->accept(this);
    if (r.isNotNull()) {
      lisps.emplace_back(c->getText(), r.as<std::string>());
    }
  }
  return lisps;
}

antlrcpp::Any HelixLispyCreator::visitExpr(HelixParser::ExprContext *ctx) {
  return visit(ctx->e());
}

antlrcpp::Any HelixLispyCreator::visitNl(HelixParser::NlContext * /*ctx*/) {
  return {};
}

antlrcpp::Any HelixLispyCreator::visitBind(HelixParser::BindContext *ctx) {
  return fmt::format(FMT_STRING("(BIND {} {})"), ctx->ID()->getText(),
                     visit(ctx->e()).as<std::string>());
}

antlrcpp::Any HelixLispyCreator::visitAddSub(HelixParser::AddSubContext *ctx) {
  const auto a0 = std::move(visit(ctx->e(0)).as<std::string>());
  const auto a1 = std::move(visit(ctx->e(1)).as<std::string>());
  if (ctx->SUB() != nullptr) {
    return fmt::format(FMT_STRING("(ADD {} (NEG {}))"), a0, a1);
  }
  return fmt::format(FMT_STRING("(ADD {} {})"), a0, a1);
}

antlrcpp::Any HelixLispyCreator::visitDivMul(HelixParser::DivMulContext *ctx) {
  const auto a0 = std::move(visit(ctx->e(0)).as<std::string>());
  const auto a1 = std::move(visit(ctx->e(1)).as<std::string>());
  if (ctx->DIV() != nullptr) {
    return fmt::format(FMT_STRING("(MUL {} (INV {}))"), a0, a1);
  }
  return fmt::format(FMT_STRING("(MUL {} {})"), a0, a1);
}

antlrcpp::Any HelixLispyCreator::visitDouble(HelixParser::DoubleContext *ctx) {
  return fmt::format(FMT_STRING("(DOUBLE {})"), ctx->DOUBLE()->getText());
}

antlrcpp::Any HelixLispyCreator::visitInt(HelixParser::IntContext *ctx) {
  return fmt::format(FMT_STRING("(INT {})"), ctx->INT()->getText());
}

antlrcpp::Any
HelixLispyCreator::visitFloat(HelixParser::FloatContext *ctx) {
  const std::string s = ctx->FLOAT()->getText();
  return fmt::format(FMT_STRING("(FLOAT {})"), s.substr(0, s.size()-1));
}

antlrcpp::Any HelixLispyCreator::visitBool(HelixParser::BoolContext *ctx) {
  return fmt::format(FMT_STRING("(BOOL {})"), ctx->BOOL()->getText());
}

antlrcpp::Any
HelixLispyCreator::visitIdentifier(HelixParser::IdentifierContext *ctx) {
  return fmt::format(FMT_STRING("(ID {})"), ctx->ID()->getText());
}

antlrcpp::Any
HelixLispyCreator::visitCompare(HelixParser::CompareContext *ctx) {
  static std::unordered_map<size_t, std::string_view> m = {
      {HelixParser::GE, "GE"}, {HelixParser::LE, "LE"}, {HelixParser::EQ, "EQ"},
      {HelixParser::NE, "NE"}, {HelixParser::LT, "LT"}, {HelixParser::GT, "GT"},
  };
  return fmt::format(FMT_STRING("({} {} {})"), m[ctx->op->getType()],
                     visit(ctx->e(0)).as<std::string>(),
                     visit(ctx->e(1)).as<std::string>());
}

antlrcpp::Any HelixLispyCreator::visitBinary(HelixParser::BinaryContext *ctx) {
  static std::unordered_map<size_t, std::string_view> m = {
      {HelixParser::AND, "AND"},
      {HelixParser::OR, "OR"},
  };
  return fmt::format(FMT_STRING("({} {} {})"), m[ctx->op->getType()],
                     visit(ctx->e(0)).as<std::string>(),
                     visit(ctx->e(1)).as<std::string>());
}

antlrcpp::Any HelixLispyCreator::visitIf(HelixParser::IfContext *ctx) {
  return fmt::format(
      FMT_STRING("(IF {} {} {})"), visit(ctx->e(0)).as<std::string>(),
      visit(ctx->e(1)).as<std::string>(), visit(ctx->e(2)).as<std::string>());
}

antlrcpp::Any
HelixLispyCreator::visitBinaryNeg(HelixParser::BinaryNegContext *ctx) {
  return fmt::format(FMT_STRING("(NOT {})"), visit(ctx->e()).as<std::string>());
}

antlrcpp::Any HelixLispyCreator::visitParen(HelixParser::ParenContext *ctx) {
  return visit(ctx->e()).as<std::string>();
}
