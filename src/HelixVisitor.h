
// Generated from Helix.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "HelixParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by HelixParser.
 */
class  HelixVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by HelixParser.
   */
    virtual antlrcpp::Any visitCu(HelixParser::CuContext *context) = 0;

    virtual antlrcpp::Any visitExpr(HelixParser::ExprContext *context) = 0;

    virtual antlrcpp::Any visitNl(HelixParser::NlContext *context) = 0;

    virtual antlrcpp::Any visitBind(HelixParser::BindContext *context) = 0;

    virtual antlrcpp::Any visitAddSub(HelixParser::AddSubContext *context) = 0;

    virtual antlrcpp::Any visitDouble(HelixParser::DoubleContext *context) = 0;

    virtual antlrcpp::Any visitInt(HelixParser::IntContext *context) = 0;

    virtual antlrcpp::Any visitFloat(HelixParser::FloatContext *context) = 0;

    virtual antlrcpp::Any visitIdentifier(HelixParser::IdentifierContext *context) = 0;

    virtual antlrcpp::Any visitBool(HelixParser::BoolContext *context) = 0;

    virtual antlrcpp::Any visitCompare(HelixParser::CompareContext *context) = 0;

    virtual antlrcpp::Any visitBinary(HelixParser::BinaryContext *context) = 0;

    virtual antlrcpp::Any visitDivMul(HelixParser::DivMulContext *context) = 0;

    virtual antlrcpp::Any visitIf(HelixParser::IfContext *context) = 0;

    virtual antlrcpp::Any visitBinaryNeg(HelixParser::BinaryNegContext *context) = 0;

    virtual antlrcpp::Any visitParen(HelixParser::ParenContext *context) = 0;


};

