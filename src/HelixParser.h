
// Generated from Helix.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"




class  HelixParser : public antlr4::Parser {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, BIND = 4, MUL = 5, DIV = 6, ADD = 7, SUB = 8, 
    GE = 9, LE = 10, EQ = 11, GT = 12, LT = 13, NE = 14, NOT = 15, AND = 16, 
    OR = 17, LB = 18, RB = 19, LP = 20, RP = 21, BOOL = 22, ID = 23, FLOAT = 24, 
    INT = 25, DOUBLE = 26, STRING = 27, LINE_COMMENT = 28, COMMENT = 29, 
    NL = 30, WS = 31
  };

  enum {
    RuleCu = 0, RuleLine = 1, RuleE = 2
  };

  HelixParser(antlr4::TokenStream *input);
  ~HelixParser();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class CuContext;
  class LineContext;
  class EContext; 

  class  CuContext : public antlr4::ParserRuleContext {
  public:
    CuContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *EOF();
    std::vector<LineContext *> line();
    LineContext* line(size_t i);


    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
   
  };

  CuContext* cu();

  class  LineContext : public antlr4::ParserRuleContext {
  public:
    LineContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    LineContext() = default;
    void copyFrom(LineContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  ExprContext : public LineContext {
  public:
    ExprContext(LineContext *ctx);

    EContext *e();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  NlContext : public LineContext {
  public:
    NlContext(LineContext *ctx);

    antlr4::tree::TerminalNode *NL();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  LineContext* line();

  class  EContext : public antlr4::ParserRuleContext {
  public:
    EContext(antlr4::ParserRuleContext *parent, size_t invokingState);
   
    EContext() = default;
    void copyFrom(EContext *context);
    using antlr4::ParserRuleContext::copyFrom;

    virtual size_t getRuleIndex() const override;

   
  };

  class  BindContext : public EContext {
  public:
    BindContext(EContext *ctx);

    antlr4::tree::TerminalNode *ID();
    antlr4::tree::TerminalNode *BIND();
    EContext *e();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  AddSubContext : public EContext {
  public:
    AddSubContext(EContext *ctx);

    antlr4::Token *op = nullptr;
    std::vector<EContext *> e();
    EContext* e(size_t i);
    antlr4::tree::TerminalNode *ADD();
    antlr4::tree::TerminalNode *SUB();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  DoubleContext : public EContext {
  public:
    DoubleContext(EContext *ctx);

    antlr4::tree::TerminalNode *DOUBLE();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  IntContext : public EContext {
  public:
    IntContext(EContext *ctx);

    antlr4::tree::TerminalNode *INT();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  FloatContext : public EContext {
  public:
    FloatContext(EContext *ctx);

    antlr4::tree::TerminalNode *FLOAT();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  IdentifierContext : public EContext {
  public:
    IdentifierContext(EContext *ctx);

    antlr4::tree::TerminalNode *ID();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  BoolContext : public EContext {
  public:
    BoolContext(EContext *ctx);

    antlr4::tree::TerminalNode *BOOL();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  CompareContext : public EContext {
  public:
    CompareContext(EContext *ctx);

    antlr4::Token *op = nullptr;
    std::vector<EContext *> e();
    EContext* e(size_t i);
    antlr4::tree::TerminalNode *GE();
    antlr4::tree::TerminalNode *LE();
    antlr4::tree::TerminalNode *EQ();
    antlr4::tree::TerminalNode *GT();
    antlr4::tree::TerminalNode *LT();
    antlr4::tree::TerminalNode *NE();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  BinaryContext : public EContext {
  public:
    BinaryContext(EContext *ctx);

    antlr4::Token *op = nullptr;
    std::vector<EContext *> e();
    EContext* e(size_t i);
    antlr4::tree::TerminalNode *AND();
    antlr4::tree::TerminalNode *OR();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  DivMulContext : public EContext {
  public:
    DivMulContext(EContext *ctx);

    antlr4::Token *op = nullptr;
    std::vector<EContext *> e();
    EContext* e(size_t i);
    antlr4::tree::TerminalNode *DIV();
    antlr4::tree::TerminalNode *MUL();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  IfContext : public EContext {
  public:
    IfContext(EContext *ctx);

    std::vector<EContext *> e();
    EContext* e(size_t i);

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  BinaryNegContext : public EContext {
  public:
    BinaryNegContext(EContext *ctx);

    antlr4::tree::TerminalNode *NOT();
    EContext *e();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  class  ParenContext : public EContext {
  public:
    ParenContext(EContext *ctx);

    antlr4::tree::TerminalNode *LP();
    EContext *e();
    antlr4::tree::TerminalNode *RP();

    virtual antlrcpp::Any accept(antlr4::tree::ParseTreeVisitor *visitor) override;
  };

  EContext* e();
  EContext* e(int precedence);

  virtual bool sempred(antlr4::RuleContext *_localctx, size_t ruleIndex, size_t predicateIndex) override;
  bool eSempred(EContext *_localctx, size_t predicateIndex);

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

