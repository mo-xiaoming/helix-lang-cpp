grammar Helix;
import HelixLexerRules;

cu : line* EOF
   ;

line : e    # Expr
     | NL   # Nl
     ;

e : 'if' e 'then' e 'else' e             # If
  | e op=(GE | LE | EQ | GT | LT | NE) e # Compare
  | e op=(AND | OR) e                    # Binary
  | NOT e                                # BinaryNeg
  | e op=(DIV | MUL) e                   # DivMul
  | e op=(ADD | SUB) e                   # AddSub
  | <assoc=right> ID BIND e              # Bind
  | LP e RP                              # Paren
  | ID                                   # Identifier
  | FLOAT                                # Float
  | DOUBLE                               # Double
  | INT                                  # Int
  | BOOL                                 # Bool
  ;
