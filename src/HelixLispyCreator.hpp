#pragma once

#include "HelixBaseVisitor.h"

struct HelixLispyCreator : HelixBaseVisitor {
  antlrcpp::Any visitCu(HelixParser::CuContext *ctx) override;

  antlrcpp::Any visitExpr(HelixParser::ExprContext *ctx) override;

  antlrcpp::Any visitNl(HelixParser::NlContext *ctx) override;

  antlrcpp::Any visitBind(HelixParser::BindContext *ctx) override;

  antlrcpp::Any visitAddSub(HelixParser::AddSubContext *ctx) override;

  antlrcpp::Any visitDivMul(HelixParser::DivMulContext *ctx) override;

  antlrcpp::Any visitDouble(HelixParser::DoubleContext *ctx) override;

  antlrcpp::Any visitInt(HelixParser::IntContext *ctx) override;

  antlrcpp::Any visitFloat(HelixParser::FloatContext *ctx) override;

  antlrcpp::Any visitBool(HelixParser::BoolContext *ctx) override;

  antlrcpp::Any visitIdentifier(HelixParser::IdentifierContext *ctx) override;

  antlrcpp::Any visitCompare(HelixParser::CompareContext *ctx) override;

  antlrcpp::Any visitBinary(HelixParser::BinaryContext *ctx) override;

  antlrcpp::Any visitIf(HelixParser::IfContext *ctx) override;

  antlrcpp::Any visitBinaryNeg(HelixParser::BinaryNegContext *ctx) override;

  antlrcpp::Any visitParen(HelixParser::ParenContext *ctx) override;
};
