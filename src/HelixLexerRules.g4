lexer grammar HelixLexerRules;

BIND : '=';

MUL : '*';
DIV : '/';
ADD : '+';
SUB : '-';

GE : 'ge';
LE : 'le';
EQ : 'eq';
GT : 'gt';
LT : 'lt';
NE : 'ne';

NOT: 'not';
AND: 'and';
OR : 'or';

LB  : '{';
RB  : '}';

LP  : '(';
RP  : ')';

BOOL : 'true' | 'false';

ID  : [a-zA-Z][a-zA-Z_]*;

FLOAT  : DOUBLE 'f';
INT    : '-'? DIGIT+;
DOUBLE : '-'? DIGIT+ '.' DIGIT*
       | '-'? '.' DIGIT+
       ;

fragment DIGIT : [0-9];

STRING: '"' (ESC|.)*? '"';
fragment ESC: '\\"' | '\\\\';

LINE_COMMENT: '//' .*? NL -> skip;
COMMENT : '/*' .*? '*/' -> skip;

NL : ('\r'? '\n') | '\r';

WS : [ \t] -> skip;

