{ nixpkgs ? import <nixpkgs> {} }:

with nixpkgs;
let
  drv-attrs = {
    pname = "helix-lang-cpp";
    version = "0.0.1";

    src = ./.;

    nativeBuildInputs = [ cmake ninja antlr ];

    buildInputs = [ spdlog doctest antlr.runtime.cpp ];

    doCheck = true;
    checkPhase = "ctest --output-on-failure";

    ANTLR4_INCLUDE_PATH = "${antlr.runtime.cpp.dev}/include/antlr4-runtime";
    UBSAN_OPTIONS="print_stacktrace=1";
    ASAN_OPTIONS="detect_leaks=1:strict_string_checks=1:detect_stack_use_after_return=1:check_initialization_order=1:strict_init_order=1:use_odr_indicator=1";
  };
in
rec {
  g = gcc11Stdenv.mkDerivation drv-attrs;
  c = (llvmPackages_13.stdenv.mkDerivation drv-attrs).overrideAttrs (oa: {
    CPATH = lib.makeSearchPathOutput "dev" "include" oa.buildInputs;
  });
}
